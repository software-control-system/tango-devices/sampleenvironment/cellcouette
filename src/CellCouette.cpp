static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Instrumentation/CellCouette/src/CellCouette.cpp,v 1.39 2010-03-26 09:16:38 vince_soleil Exp $";
//+=============================================================================
//
// file :         CellCouette.cpp
//
// description :  C++ source for the CellCouette and its commands. 
//                The class is derived from Device. It represents the
//                CORBA servant object which will be accessed from the
//                network. All commands which can be executed on the
//                CellCouette are implemented in this file.
//
// project :      TANGO Device Server
//
// $Author: vince_soleil $
//
// $Revision: 1.39 $
//
// $Log: not supported by cvs2svn $
// Revision 1.38  2009/05/28 10:33:31  buteau
// - test avec equipe LPS et modif des formules
//
// Revision 1.37  2009/05/25 17:18:28  buteau
// MANTIS 10839
//
// Revision 1.36  2009/03/10 15:19:20  buteau
// cout en DEBUG_STREAM
// Test des nouvelles formules faits avec F. Meneau
// bug 10839
// a valider par les post-doc
//
// Revision 1.34  2008/12/10 16:31:31  jean_coquet
// must not call speed-calculation outside dev_state!
// removed my added calls
//
// Revision 1.33  2008/12/08 14:06:57  jean_coquet
// changed torque calculation
// added attribute viscosity
// changed stress calculation
//
// Revision 1.32  2008/08/08 12:22:43  julien_malik
// compil VC8
//
// Revision 1.31  2008/02/20 15:13:32  pierrejoseph
// stain has been renamed in stress (requested by user)
//
// Revision 1.30  2008/02/19 10:42:21  sebleport
// - on line corrected to ensure compilation under linux
//
// Revision 1.29  2008/02/15 10:11:30  pierrejoseph
// Apply some logs suppression already done by seb - cast pb
//
// Revision 1.28  2008/02/14 16:26:20  pierrejoseph
// work with temporary variable in the start and set_motor_param methods.
//
// Revision 1.27  2008/02/14 15:46:18  pierrejoseph
// sheare_rate calculation has been reviewed
//
// Revision 1.26  2008/02/14 15:12:12  pierrejoseph
// stress = stress * 1000 because torque is in �Nm
//
// Revision 1.25  2008/02/14 10:35:43  sebleport
// - some corrections to compile under linux
//
// Revision 1.24  2008/02/11 15:41:51  sebleport
// - bug corrected in write_speedUnit() and write_frequency
//
// Revision 1.22  2008/02/07 14:29:25  sebleport
// const int replaced by const double type for STEP_DEGREE_RATIO constant
//
// Revision 1.21  2008/02/07 13:10:52  sebleport
// - compilation OK
//
// Revision 1.20  2008/02/06 18:27:52  buteau
// relecture crois�e de code
//
// Revision 1.19  2008/02/05 16:40:19  sebleport
// - code revisited
//
// Revision 1.18  2008/02/04 17:36:47  sebleport
// - come sleep time have changed...
//
// Revision 1.17  2008/02/01 17:08:32  sebleport
// - halfAngularAmplitude is no more a polled attribute. finallly we don't read this value on the hardware.
// - some bugs about unity conversions are corrected
//
// Revision 1.16  2008/02/01 11:49:56  sebleport
// - pollinn values have been modified
// temperature: 30000 ms
// weight: 1000 ms
// halfAngularAmplitude: 30000ms
//
// Revision 1.15  2008/01/31 10:13:12  sebleport
// - no message
//
// Revision 1.14  2008/01/29 16:53:15  sebleport
// - halfAngular, state, temperature, weight attributes are polled.
// - the device server well work with ATK PANEL client
//
// Revision 1.13  2008/01/29 14:01:49  sebleport
// - documenation modofied
// - resolution choice is now 1 or 64
//
// Revision 1.12  2008/01/25 18:29:28  sebleport
// - add motorResolution attiributes:
// - timeRamp & speedMaxRamp are now R/W.
//
// Revision 1.11  2008/01/24 18:35:25  sebleport
// - sendMovementLawdata() replaced by setMotorParam()
// - descrtiption command / attributes have changed
// - speed calculation is done in dev_state() by using a polling method
//
// Revision 1.10  2008/01/23 11:00:27  sebleport
// - add GetMotorState command
//
// Revision 1.9  2008/01/22 18:00:41  sebleport
// - write_position() and read position() work
//
// Revision 1.8  2008/01/18 18:23:20  sebleport
// - all commands and attributes are implemented. device almost entirely tested.
//
// Revision 1.7  2008/01/18 15:52:31  sebleport
// - status management has changed
//
// Revision 1.6  2008/01/17 18:29:23  sebleport
// - communication part for the torque controller is now working
// - almost all attribute methodes implemented
// - device don't tested
//
// Revision 1.5  2008/01/16 19:55:39  sebleport
// - communication part for the torque controller is now working
// - almost all attribute methodes implemented
// - device don't tested
//
// Revision 1.4  2008/01/15 19:48:38  sebleport
// communication part for temperature and motor controllers is finsished, but not yet for the torque controller
//
// Revision 1.3  2008/01/09 17:26:04  sebleport
// - communication part is implemented but not yet tested
//
// Revision 1.2  2007/12/21 14:56:24  sebleport
// - add Makefile.linux and Makefile.vc files
//
// Revision 1.1  2007/12/17 16:32:08  stephle
// initial import
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================



//===================================================================
//
//	The following table gives the correspondance
//	between commands and method's name.
//
//  Command's name |  Method's name
//	----------------------------------------
//  State          |  dev_state()
//  Status         |  dev_status()
//  Start          |  start()
//  Stop           |  stop()
//  PowerOFF       |  power_off()
//  Reset          |  reset()
//  SendCommand    |  send_command()
//  SetMotorParam  |  set_motor_param()
//  GetMotorParam  |  get_motor_param()
//  GetMotorState  |  get_motor_state()
//
//===================================================================


#include <tango.h>
#include <omnithread.h>
#include <CellCouette.h>
#include <CellCouetteClass.h>

const size_t MOTOR = 15;
const size_t TORQUE_SENSOR = 2;
const size_t TEMPERATURE_SENSOR = 3;
const int SLEEPING_TIME = 400000000;    // in nano seconds

#define Pi 3.14159265
const double STEP_DEG_RATIO= 0.036;
// NB de pas par tour de codeur
#define STEP_PER_ROTATION 10000

enum
{
	CONTINUOUS,
		OSCILLATIONS
}MODE;

enum
{
	STEP_PER_SECOND,
		HERTZ,
		ROTATION_PER_MINUTE,
		RADIAN_PER_SECOND,
		SHEAR_RATE,
}SPEED_UNITY;

enum
{
	STEP,
		DEGREE
}POSITON_UNITY;

namespace CellCouette_ns
{
	
	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::CellCouette(string &s)
	// 
	// description : 	constructor for simulated CellCouette
	//
	// in : - cl : Pointer to the DeviceClass object
	//      - s : Device name 
	//
	//-----------------------------------------------------------------------------
	CellCouette::CellCouette(Tango::DeviceClass *cl,string &s)
		:Tango::Device_4Impl(cl,s.c_str())
	{
		attr_speedUnity_write = 0;
		init_device();
	}
	
	CellCouette::CellCouette(Tango::DeviceClass *cl,const char *s)
		:Tango::Device_4Impl(cl,s)
	{
		attr_speedUnity_write = 0;
		init_device();
	}
	
	CellCouette::CellCouette(Tango::DeviceClass *cl,const char *s,const char *d)
		:Tango::Device_4Impl(cl,s,d)
	{
		attr_speedUnity_write = 0;
		init_device();
	}
	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::delete_device()
	// 
	// description : 	will be called at device destruction or at init command.
	//
	//-----------------------------------------------------------------------------
	void CellCouette::delete_device()
	{
		//	Delete device's allocated object
		
		DELETE_SCALAR_ATTRIBUTE(attr_position_read);
		DELETE_SCALAR_ATTRIBUTE(attr_isPositiveRotation_read);
		DELETE_SCALAR_ATTRIBUTE(attr_speed_read);
		DELETE_SCALAR_ATTRIBUTE(attr_viscosity_read);
		
		if(serial_proxy)
		{
			delete serial_proxy;
			serial_proxy = 0;
		}
	}
	
	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::init_device()
	//
	// description : 	will be called at device initialization.
	//
	//-----------------------------------------------------------------------------
	void CellCouette::init_device()
	{
		INFO_STREAM << "CellCouette::CellCouette() create device " << device_name << endl;
		
		// Initialise variables to default values
		//--------------------------------------------
		get_device_property();
		
		CREATE_SCALAR_ATTRIBUTE(attr_position_read);
		CREATE_SCALAR_ATTRIBUTE(attr_isPositiveRotation_read);
		CREATE_SCALAR_ATTRIBUTE(attr_speed_read);
		CREATE_SCALAR_ATTRIBUTE(attr_viscosity_read);
		
		serial_proxy = 0;
		
		temperature = 0;
		torque = 0;
		stress = 0;
		cell_radius = 0;
		cell_height = 0;
		weight_offset = 0;
		weight = 0;
		timeRamp = 0;
		periode = 0;
		speedMaxRamp = 0;
		deformation = 0;
		speed_max = 0;
		cell_gap = 0;
		frequency = 1;
		resolution = 1;
		old_position = 0;
		old_time = 0;
		first_speed_measurement = true;
    raw_speed = 0.;
   speed_setpoint_in_hertz=0;
	}
	
	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::get_device_property()
	//
	// description : 	Read the device properties from database.
	//
	//-----------------------------------------------------------------------------
	void CellCouette::get_device_property()
	{
		//	Initialize your default values here (if not done with  POGO).
		//------------------------------------------------------------------
		
		//	Read device properties from database.(Automatic code generation)
		//------------------------------------------------------------------
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("SerialProxyName"));
	dev_prop.push_back(Tango::DbDatum("TorqueCalibrationCoefficient"));

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_device()->get_property(dev_prop);
	Tango::DbDatum	def_prop, cl_prop;
	CellCouetteClass	*ds_class =
		(static_cast<CellCouetteClass *>(get_device_class()));
	int	i = -1;

	//	Try to initialize SerialProxyName from class property
	cl_prop = ds_class->get_class_property(dev_prop[++i].name);
	if (cl_prop.is_empty()==false)	cl_prop  >>  serialProxyName;
	//	Try to initialize SerialProxyName from default device value
	def_prop = ds_class->get_default_device_property(dev_prop[i].name);
	if (def_prop.is_empty()==false)	def_prop  >>  serialProxyName;
	//	And try to extract SerialProxyName value from database
	if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  serialProxyName;

	//	Try to initialize TorqueCalibrationCoefficient from class property
	cl_prop = ds_class->get_class_property(dev_prop[++i].name);
	if (cl_prop.is_empty()==false)	cl_prop  >>  torqueCalibrationCoefficient;
	//	Try to initialize TorqueCalibrationCoefficient from default device value
	def_prop = ds_class->get_default_device_property(dev_prop[i].name);
	if (def_prop.is_empty()==false)	def_prop  >>  torqueCalibrationCoefficient;
	//	And try to extract TorqueCalibrationCoefficient value from database
	if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  torqueCalibrationCoefficient;



		//	End of Automatic code generation
		//------------------------------------------------------------------
		
		if(serialProxyName.empty()==true)
		{
			INFO_STREAM<<"serialProxyName property is missing"<<endl;
		}
		else
		{
			INFO_STREAM<<"serialProxyName property: "<<serialProxyName<<endl;
		}
	}
	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::always_executed_hook()
	// 
	// description : 	method always executed before any command is executed
	//
	//-----------------------------------------------------------------------------
	void CellCouette::always_executed_hook()
	{
		
	}
	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::read_attr_hardware
	// 
	// description : 	Hardware acquisition for attributes.
	//
	//-----------------------------------------------------------------------------
	void CellCouette::read_attr_hardware(vector<long> &attr_list)
	{
		DEBUG_STREAM << "CellCouette::read_attr_hardware(vector<long> &attr_list) entering... "<< endl;
		//	Add your own code here
	}
//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_viscosity
// 
// description : 	Extract real attribute values for viscosity acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_viscosity(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_viscosity(Tango::Attribute &attr) entering... "<< endl;

  //- calculate parts of equation
  double tmp1 = 0.;
  double tmp2 = 0.;
  double tmp3 = 0.;
  double denominateur = 0.;
  double tmp4 = 0.;
 //- calculate speed in rad/sec.
  //- time_t ltime;
  //- time (&ltime);
  //- long time = ltime;
  //- this->speed_calculation(time, motor_status.position);

// check if cell is really moving
if (raw_speed == 0)
	return;
else
{
// calculations are based on the speed setpoint rather than read speed which is not reliable
//
// calculations are based on the the speed expressed in Hertz, which depends on the speed unity !
	update_speed_setpoint_in_hertz();

	DEBUG_STREAM  << "speed_setpoint_in_hertz " << speed_setpoint_in_hertz            << endl;


if (speed_setpoint_in_hertz == 0.)
	return;

  tmp1 = (1000.0 * torque) / ((4 * Pi) * (2* Pi* speed_setpoint_in_hertz));
  tmp2 = (cell_radius + cell_gap)*(cell_radius + cell_gap);
  denominateur = (tmp2 - (cell_radius *cell_radius));

	if (denominateur == 0.)
      return;

  tmp3 = (cell_height * cell_radius * cell_radius * tmp2) /denominateur;
  tmp4 = (pow(cell_radius, 4.0) ) / (4.232 * (18.0 - cell_height));
 
// 
 DEBUG_STREAM  << "torque : " << torque 
            << "   tmp1 " << tmp1 
            << endl;
  DEBUG_STREAM << "cell_height : " << cell_height 
            << "   cell_radius : " << cell_radius 
            << "   cell_gap : " << cell_gap 
            << "   tmp2 " << tmp2 
            << endl;

DEBUG_STREAM << "   tmp3 " << tmp3 << endl;
DEBUG_STREAM << "   tmp4 " << tmp4 << endl;
  //- finally : the viscosity

	if ((tmp3+ tmp4) == 0.)
      return;

  *attr_viscosity_read  = tmp1 * (1.0 / (tmp3 + tmp4));

  attr.set_value(attr_viscosity_read);
}

}


	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::read_cellHeight
	// 
	// description : 	Extract real attribute values for cellHeight acquisition result.
	//
	//-----------------------------------------------------------------------------
	void CellCouette::read_cellHeight(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "CellCouette::read_cellHeight(Tango::Attribute &attr) entering... "<< endl;
	}
	
	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::write_cellHeight
	// 
	// description : 	Write cellHeight attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void CellCouette::write_cellHeight(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "CellCouette::write_cellHeight(Tango::WAttribute &attr) entering... "<< endl;
		
		attr.get_write_value(cell_height);
	
	}
	
	
	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::read_halfAngularAmplitude
	// 
	// description : 	Extract real attribute values for halfAngularAmplitude acquisition result.
	//
	//-----------------------------------------------------------------------------
	void CellCouette::read_halfAngularAmplitude(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "CellCouette::read_halfAngularAmplitude(Tango::Attribute &attr) entering... "<< endl;
		
		if(attr_mode_write==OSCILLATIONS)
		{
			attr.set_value(&attr_halfAngularAmplitude_write);
			attr.set_quality(Tango::ATTR_VALID);
		}
		else
		{
			attr.set_quality(Tango::ATTR_INVALID);
		}
	}
	
	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::write_halfAngularAmplitude
	// 
	// description : 	Write halfAngularAmplitude attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void CellCouette::write_halfAngularAmplitude(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "CellCouette::write_halfAngularAmplitude(Tango::WAttribute &attr) entering... "<< endl;
		
		if(attr_mode_write == OSCILLATIONS)
		{
			string theta_in_step;
			
			attr.get_write_value(attr_halfAngularAmplitude_write);
			
			// update deformation
			if ( cell_gap !=0)
			{
				deformation = (attr_halfAngularAmplitude_write) * (cell_radius + cell_gap) / cell_gap;
			}
			// else : On ne touche pas � la valeur de   deformation
			
			
			// DEGREE
			if(attr_positionUnity_write == 1)
			{
				theta_in_step = XString<long>::convertToString(long(attr_halfAngularAmplitude_write / STEP_DEG_RATIO));
				DEBUG_STREAM<<"theta_in_step = "<<theta_in_step<<endl;
			}
			// STEPS
			else
			{
				theta_in_step = XString<long>::convertToString(attr_halfAngularAmplitude_write);
			}
			// "PO # n := X", set the variable number "n" to the value "X"
			// possible error code:
			// 0 : false variable number
			// 1 : "X" exceeds the maximum value
			
			string cmd_theta1 = "15PO #2 := +" + theta_in_step;
			string cmd_theta2 = "15PO #3 := -" + theta_in_step;
			
			_DEV_TRY(
				controller_response = write_read_on_serial_port(cmd_theta1),
				"write absoltue position to theta plus",
				"write_halfAngularAmplitude");
			
			_DEV_TRY(
				controller_response = write_read_on_serial_port(cmd_theta2),
				"write absolute position to theta minus",
				"write_halfAngularAmplitude");
		}
		else if(attr_mode_write == CONTINUOUS)
		{
			Tango::Except::throw_exception(
				(const char*) "TANGO_CONFIGURATION_ERROR",
				(const char*) "IMPOSSIBLE to set this attribute in CONTINUOUS mode",
				(const char*) "CellCouette::write_halfAngularAmplitude()");  
		}
	}
	
	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::read_speedUnity
	// 
	// description : 	Extract real attribute values for speedUnity acquisition result.
	//
	//-----------------------------------------------------------------------------
	void CellCouette::read_speedUnity(Tango::Attribute &attr)
	{
		DEBUG_STREAM << "CellCouette::read_speedUnity(Tango::Attribute &attr) entering... "<< endl;
	}
	
	//+----------------------------------------------------------------------------
	//
	// method : 		CellCouette::write_speedUnity
	// 
	// description : 	Write speedUnity attribute values to hardware.
	//
	//-----------------------------------------------------------------------------
	void CellCouette::write_speedUnity(Tango::WAttribute &attr)
	{
		DEBUG_STREAM << "CellCouette::write_speedUnity(Tango::WAttribute &attr) entering... "<< endl;
		
		Tango::DevUShort tmp;
		double convertValue = 0;

		attr.get_write_value(tmp);
		
		if(tmp == 0 || tmp == 1 || tmp == 2 || tmp == 3 || tmp == 4)
		{
			// change the speed unity imply to recalculate the "speed" attributes
			// the concerned attributes are: speed, speedMaxRamp, speedMax
			
			// at this moment, attr_speedUnity_write is the old speed unity
			switch(attr_speedUnity_write)
			{
			case STEP_PER_SECOND:
				// new speed unity
				{
			 switch(tmp)
				{
				case HERTZ:
					attr_speed_write = attr_speed_write / STEP_PER_ROTATION;
					speedMaxRamp = speedMaxRamp / STEP_PER_ROTATION;
					speed_max = speed_max / STEP_PER_ROTATION;
					break;
					
				case ROTATION_PER_MINUTE:
					attr_speed_write = (attr_speed_write / STEP_PER_ROTATION) * 60;
					speedMaxRamp = (speedMaxRamp / STEP_PER_ROTATION) * 60;
					speed_max = speed_max / STEP_PER_ROTATION * 60;
					break;
					
				case RADIAN_PER_SECOND:
					attr_speed_write = (attr_speed_write / STEP_PER_ROTATION) * 2 * Pi;
					speedMaxRamp = long((speedMaxRamp / STEP_PER_ROTATION) * 2 * Pi);
					speed_max = (speed_max / STEP_PER_ROTATION) * 2 * Pi;
					break;
					
				case SHEAR_RATE:
					if(cell_gap != 0) {
						convertValue = (2 * Pi / STEP_PER_ROTATION) * ((cell_radius + cell_gap) / cell_gap);
					}

					attr_speed_write = attr_speed_write * convertValue;
					speedMaxRamp = long(speedMaxRamp * convertValue);
					speed_max = speed_max * convertValue;
					break;
					
				default:
					{
						ERROR_STREAM <<" wrong speed unit selected"<<endl;
						Tango::Except::throw_exception (
							(const char *)"DATA_OUT_OF_RANGE",
							(const char *)"wrong speed unit selected, old unit was STEP_PER_SECOND",
							(const char *)"CellCouette::write_speedUnity");
				      // no-op break;
					}
				}
			break;   
				} // end case STEP_PER_SECOND
				case HERTZ:
					{
					// new speed unity
					switch(tmp)
					{
					case STEP_PER_SECOND:
						attr_speed_write = attr_speed_write * STEP_PER_ROTATION;
						speedMaxRamp = speedMaxRamp * STEP_PER_ROTATION;
						speed_max = speed_max * STEP_PER_ROTATION;
						break;
						
					case ROTATION_PER_MINUTE:
						attr_speed_write = attr_speed_write * 60;
						speedMaxRamp = speedMaxRamp * 60;
						speed_max = speed_max * 60;
						break;
						
					case RADIAN_PER_SECOND:
						attr_speed_write = attr_speed_write * 2 * Pi;
						speedMaxRamp = long(speedMaxRamp * 2 * Pi);
						speed_max = speed_max * 2 * Pi;
						break;
						
					case SHEAR_RATE:
						if(cell_gap != 0) {
							convertValue = (cell_radius + cell_gap) / cell_gap;
						}
						attr_speed_write = attr_speed_write * 2 * Pi * convertValue;
						speedMaxRamp = long(speedMaxRamp * 2 * Pi * convertValue);
						speed_max = speed_max * 2 * Pi * convertValue;
						break;
						
					default:
						{
							ERROR_STREAM <<" wrong speed unit selected"<<endl;
							Tango::Except::throw_exception (
								(const char *)"DATA_OUT_OF_RANGE",
								(const char *)"wrong speed unit selected, old unit was HERTZ",
								(const char *)"CellCouette::write_speedUnity");
						}
					}
					break;
					}
				case ROTATION_PER_MINUTE:
						// new speed unity
					{
							switch(tmp)
						{
						case STEP_PER_SECOND:
							attr_speed_write = (attr_speed_write * STEP_PER_ROTATION) / 60;
							speedMaxRamp = (speedMaxRamp * STEP_PER_ROTATION) / 60;
							speed_max = (speed_max * STEP_PER_ROTATION) / 60;
							
							break;
							
						case HERTZ:
							attr_speed_write = attr_speed_write / 60;
							speedMaxRamp = speedMaxRamp / 60;
							speed_max = speed_max / 60;
							
							break;
							
						case RADIAN_PER_SECOND:
							attr_speed_write = (attr_speed_write * 2 * Pi) / 60;
							speedMaxRamp = long((speedMaxRamp * 2 * Pi) / 60);
							speed_max = (speed_max * 2 * Pi) / 60;
							
							break;
							
						case SHEAR_RATE:
							if(cell_gap != 0) {
								convertValue = (cell_radius + cell_gap) / cell_gap;
							}
							attr_speed_write = attr_speed_write * 2 * Pi * convertValue / 60;
							speedMaxRamp =long( speedMaxRamp * 2 * Pi * convertValue / 60);
							speed_max = speed_max * 2 * Pi * convertValue / 60;
							break;
							
						default:
							{
								ERROR_STREAM <<" wrong speed unit selected"<<endl;
								Tango::Except::throw_exception (
									(const char *)"DATA_OUT_OF_RANGE",
									(const char *)"wrong speed unit selected, old unit was ROTATION_PER_MINUTE",
									(const char *)"CellCouette::write_speedUnity");
							}
						}
						break;
					}
						//attr_speed_write = attr_speed_write * 10000 / 60;
						case RADIAN_PER_SECOND:
							{							// new speed unity
							switch(tmp)
							{
							case STEP_PER_SECOND:
								attr_speed_write = attr_speed_write * STEP_PER_ROTATION / 2 / Pi;
								speedMaxRamp = long(speedMaxRamp * STEP_PER_ROTATION / 2 / Pi);
								speed_max = speed_max * STEP_PER_ROTATION / 2 / Pi;
								break;
								
							case HERTZ:
								attr_speed_write = attr_speed_write / 2 / Pi;
								speedMaxRamp = long(speedMaxRamp / 2 / Pi);
								speed_max = speed_max / 2 / Pi;
								break;
								
							case ROTATION_PER_MINUTE:
								attr_speed_write = attr_speed_write / 2 / Pi * 60;
								speedMaxRamp = long(speedMaxRamp / 2 / Pi * 60);
								speed_max = speed_max / 2 / Pi * 60;
								break;
								
							case SHEAR_RATE:
								if(cell_gap != 0) {
									convertValue = (cell_radius + cell_gap) / cell_gap;
								}
								
								attr_speed_write = attr_speed_write * convertValue;
								speedMaxRamp = long(speedMaxRamp * convertValue);
								speed_max = speed_max * convertValue;
								break;
								
							default:
								{
									ERROR_STREAM <<" wrong speed unit selected"<<endl;
									Tango::Except::throw_exception (
										(const char *)"DATA_OUT_OF_RANGE",
										(const char *)"wrong speed unit selected, old unit was RADIAN_PER_SECOND",
										(const char *)"CellCouette::write_speedUnity");
								}
							}
							break;
							}							
							case SHEAR_RATE:
							{
								if(cell_gap != 0) {
									convertValue = (cell_radius + cell_gap) / cell_gap;
								}
								// new speed unity
								switch(tmp)
								{
								case STEP_PER_SECOND:
									if(convertValue != 0) 
									{
										attr_speed_write = attr_speed_write * STEP_PER_ROTATION / (convertValue * 2 * Pi);
										speedMaxRamp = long(speedMaxRamp * STEP_PER_ROTATION / (convertValue * 2 * Pi));
										speed_max = speed_max * STEP_PER_ROTATION / (convertValue * 2 * Pi);
									}
									else
									{
										attr_speed_write = 0;
										speedMaxRamp = 0;
										speed_max = 0;
									}
									break;

								case HERTZ:
									if(convertValue != 0) 
									{
										attr_speed_write = attr_speed_write / (2 * Pi * convertValue);
										speedMaxRamp = long(speedMaxRamp / (2 * Pi * convertValue));
										speed_max = speed_max / (2 * Pi * convertValue);
									}
									else
									{
										attr_speed_write = 0;
										speedMaxRamp = 0;
										speed_max = 0;
									}

									break;		
									
									
								case ROTATION_PER_MINUTE:
									if(convertValue != 0) 
									{
										attr_speed_write = attr_speed_write * 60 / (2 * Pi * convertValue);
										speedMaxRamp = long(speedMaxRamp * 60 / (2 * Pi * convertValue));
										speed_max = speed_max * 60 / (2 * Pi * convertValue);
									}
									else
									{
										attr_speed_write = 0;
										speedMaxRamp = 0;
										speed_max = 0;
									}

									break;
									
								case RADIAN_PER_SECOND:
									if(convertValue != 0) 
									{
										attr_speed_write = attr_speed_write / convertValue;
										speedMaxRamp = long(speedMaxRamp / convertValue);
										speed_max = speed_max / convertValue;	
									}
									else
									{
										attr_speed_write = 0;
										speedMaxRamp = 0;
										speed_max = 0;
									}
									break;

								default:
									{
										ERROR_STREAM <<" wrong speed unit selected"<<endl;
										Tango::Except::throw_exception (
											(const char *)"DATA_OUT_OF_RANGE",
											(const char *)"wrong speed unit selected, old unit was SHEAR_RATE",
											(const char *)"CellCouette::write_speedUnity");
									}									
								}/// end switch
							}
							break;

								default:
									{
										ERROR_STREAM <<" wrong speed unit selected"<<endl;
										Tango::Except::throw_exception (
											(const char *)"DATA_OUT_OF_RANGE",
											(const char *)"old speed unit was wrong ",
											(const char *)"CellCouette::write_speedUnity");
									}

				} //end switch		
				// All checks have now been done . we can update speedUnity
			attr_speedUnity_write = tmp;
  }
  else
  {
	  // Normally we should never be here
      Tango::Except::throw_exception(
		  (const char*) "DATA_OUT_OF_RANGE",
		  (const char*) " The last speed unit was wrong, you have to choose between 0 & 4",
		  (const char*) "CellCouette::write_speedUnit()");
  }
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_positionUnity
// 
// description : 	Extract real attribute values for positionUnity acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_positionUnity(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_positionUnity(Tango::Attribute &attr) entering... "<< endl;
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_positionUnity
// 
// description : 	Write positionUnity attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_positionUnity(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_positionUnity(Tango::WAttribute &attr) entering... "<< endl;
	
	Tango::DevUShort tmp;
	
	attr.get_write_value(tmp);
	
	if((tmp == 0) || (tmp == 1))
	{
		if(attr_positionUnity_write != tmp)
		{
			// was step
			if(attr_positionUnity_write = 0)
			{
				*attr_position_read = (*attr_position_read) * STEP_DEG_RATIO;
				attr_halfAngularAmplitude_write = long(attr_halfAngularAmplitude_write * STEP_DEG_RATIO);
			}
			//was degrees
			else
			{
				*attr_position_read = (*attr_position_read) / STEP_DEG_RATIO;
				attr_halfAngularAmplitude_write = long(attr_halfAngularAmplitude_write / STEP_DEG_RATIO);
			}
			
			attr_positionUnity_write = tmp;
		}
	}
	else
	{
		Tango::Except::throw_exception(
			(const char*) "DATA_OUT_OF_RANGE",
			(const char*) " wrong position unit, you have to chosse between 0(steps) or 1(degrees)",
			(const char*) "CellCouette::write_positionUnit()");
	}
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_temperature
//
// description : 	Extract real attribute values for temperature acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_temperature(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_temperature(Tango::Attribute &attr) entering... "<< endl;
	
	_DEV_TRY(
		controller_response = write_read_on_serial_port("03M1"),
		"communicate with a controller",
		"read_temperature");
	
	if(controller_response == "no response")
	{
		DEBUG_STREAM<<" temperature controller didn't reply a consistant value"<<endl;
	}
	// update temperature
	else
	{
		temperature = XString<double>::convertFromString(controller_response);
	}
	attr.set_value(&temperature);
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_torque
// 
// description : 	Extract real attribute values for torque acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_torque(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_torque(Tango::Attribute &attr) entering... "<< endl;
	
	// torque in �Nm  (10exp-6 Nm)
  //- old formula
	//- torque = (2 * pow(10.0,-4.0) * torqueCalibrationCoefficient * (weight - weight_offset)) * 1000000;
  //- new formula torque = 209(F-F0) see bug 10839
	torque = (torqueCalibrationCoefficient * (weight - weight_offset));
DEBUG_STREAM << "torque " << torque 
     <<"    torqueCalibrationCoefficient " << torqueCalibrationCoefficient 
     << "   weight " << weight 
     << "   weight_offset " << weight_offset 
     << endl;
	attr.set_value(&torque);
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_stress
// 
// description : 	Extract real attribute values for stress acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_stress(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_stress(Tango::Attribute &attr) entering... "<< endl;
	
	// stress in Pa and torque in �Nm, cell_radius and cell_height in mm 
	//- old formula 
  //- stress = torque / (2 * Pi * cell_radius * cell_radius * cell_height) * 1000;

  //- new formula stress = viscosity * speed(rotation/sec) * ((cell radius + cell gap) / cell_gap)

  double r1_carre=0.;
  double r2_carre=0.;

  	update_speed_setpoint_in_hertz();
 

r1_carre= (cell_radius + cell_gap)* (cell_radius + cell_gap);
r2_carre = cell_radius *cell_radius;

  if ((r1_carre-r2_carre )==0.)
    return;
 
 stress = *attr_viscosity_read * 2 * Pi * speed_setpoint_in_hertz * ( r1_carre + r2_carre)/(r1_carre - r2_carre);

 attr.set_value(&stress);
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_weightOffset
// 
// description : 	Extract real attribute values for weightOffset acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_weightOffset(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_weightOffset(Tango::Attribute &attr) entering... "<< endl;
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_weightOffset
// 
// description : 	Write weightOffset attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_weightOffset(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_weightOffset(Tango::WAttribute &attr) entering... "<< endl;
	
	attr.get_write_value(weight_offset);
	
}
//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_weight
// 
// description : 	Extract real attribute values for weight acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_weight(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_weight(Tango::Attribute &attr) entering... "<< endl;
	
	_DEV_TRY(
		controller_response = write_read_on_serial_port("02N02TA*"),
		"communicate with a controller",
		"read_weigh");
	
	if(controller_response == "no response")
	{
		DEBUG_STREAM<<" torque controller didn't reply a consistant value"<<endl;
	}
	// update weight
	else
	{
		weight = XString<double>::convertFromString(controller_response);
		
		DEBUG_STREAM<<"numeric torque = "<< weight <<endl;
	}
	attr.set_value(&weight);
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_timeRamp
// 
// description : 	Extract real attribute values for timeRamp acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_timeRamp(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_timeRamp(Tango::Attribute &attr) entering... "<< endl;
	
	if(attr_mode_write == CONTINUOUS)
	{
		attr.set_value(&timeRamp);
	}
	else if(attr_mode_write == OSCILLATIONS)
	{
		// timeRamp in ms
		timeRamp = periode * (0.5 - 1/Pi);
		
		DEBUG_STREAM << "timeRamp in sec = "<< timeRamp <<endl;
		
		attr.set_value(&timeRamp);
		
		if( ((timeRamp*1000) < 1) || ((timeRamp*1000) > 65535) )
		{
			attr.set_quality(Tango::ATTR_ALARM);    
		}
	}
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_timeRamp
// 
// description : 	Write timeRamp attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_timeRamp(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_timeRamp(Tango::WAttribute &attr) entering... "<< endl;
	
	if(attr_mode_write == CONTINUOUS)
	{
		attr.get_write_value(timeRamp);
	}
	else if(attr_mode_write == OSCILLATIONS)
	{
		Tango::Except::throw_exception(
			(const char*) "DATA_OUT_OF_RANGE",
			(const char*) "IMPOSSIBLE to set this attribute in OSCILLATIONS mode",
			(const char*) "CellCouette::write_timeRamp()");  
	}
	else
	{
		Tango::Except::throw_exception(
			(const char*) "DATA_OUT_OF_RANGE",
			(const char*) " UNKNOWN operating mode selected",
			(const char*) "CellCouette::write_timeRamp()");
	}
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_speedMaxRamp
// 
// description : 	Extract real attribute values for speedMaxRamp acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_speedMaxRamp(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_speedMaxRamp(Tango::Attribute &attr) entering... "<< endl;
	
    if(attr_mode_write == CONTINUOUS)
    {
		attr.set_value(&speedMaxRamp);
    }
    else if(attr_mode_write == OSCILLATIONS)
    {
		// speedMaxRamp unity is step/s
		
		if (periode !=0)
			speedMaxRamp = long( (2 * Pi * (attr_halfAngularAmplitude_write) ) / periode);
		
		DEBUG_STREAM << "speedMaxRamp in step/s = "<< speedMaxRamp <<endl;
		
		attr.set_value(&speedMaxRamp);
    }
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_speedMaxRamp
// 
// description : 	Write speedMaxRamp attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_speedMaxRamp(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_speedMaxRamp(Tango::WAttribute &attr) entering... "<< endl;
	
	if(attr_mode_write == CONTINUOUS)
	{
		// - unity management is done in set_motor_param
		attr.get_write_value(speedMaxRamp);       
	}
	else if(attr_mode_write == OSCILLATIONS)
	{
		Tango::Except::throw_exception(
			(const char*) "DATA_OUT_OF_RANGE",
			(const char*) "IMPOSSIBLE to set this attribute in OSCILLATIONS mode",
			(const char*) "CellCouette::write_speedMaxRamp()");  
	}
	else
	{
		Tango::Except::throw_exception(
			(const char*) "DATA_OUT_OF_RANGE",
			(const char*) " UNKNOWN operating mode selected",
			(const char*) "CellCouette::write_speedMaxRamp()");  
	}
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_motorResolution
// 
// description : 	Extract real attribute values for motorResolution acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_motorResolution(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_motorResolution(Tango::Attribute &attr) entering... "<< endl;
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_motorResolution
// 
// description : 	Write motorResolution attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_motorResolution(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_motorResolution(Tango::WAttribute &attr) entering... "<< endl;
	
	unsigned short tmp;
	
	attr.get_write_value(tmp);
	
	switch(tmp)
	{
    case 1:
		resolution = 1;
		break;
    case 2:
		resolution = 2;
		break;
    case 4:
		resolution = 4;
		break;
    case 8:
		resolution = 8;
		break;
    case 16:
		resolution = 16;
		break;
    case 32:
		resolution = 32;
		break;
    case 64:
		resolution = 64;
		break;
    default:
		Tango::Except::throw_exception(
			(const char*) "DATA_OUT_OF_RANGE",
			(const char*) "BE CAREFUL ! resolution = 1, 2, 4, 8, 16, 32 or 64",
			(const char*) "CellCouette::write_motorResolution()");
	}
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_deformation
// 
// description : 	Extract real attribute values for deformation acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_deformation(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_deformation(Tango::Attribute &attr) entering... "<< endl;
	
	if(attr_mode_write == OSCILLATIONS)
	{
		attr.set_value(&deformation);
		attr.set_quality(Tango::ATTR_VALID);
	}
	else
	{
		attr.set_quality(Tango::ATTR_INVALID);
	}
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_frequency
// 
// description : 	Extract real attribute values for frequency acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_frequency(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_frequency(Tango::Attribute &attr) entering... "<< endl;
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_frequency
// 
// description : 	Write frequency attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_frequency(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_frequency(Tango::WAttribute &attr) entering... "<< endl;
	
    Tango::DevDouble tmp=1;

	attr.get_write_value(tmp);
	
	if(tmp>0)
	{
		frequency = tmp;
		periode = 1 / frequency;
	}
	else
	{	
	Tango::Except::throw_exception(
		(const char*) "DATA_OUT_OF_RANGE",
		(const char*) "BE CAREFUL ! Frequency must be positive",
		(const char*) "CellCouette::write_frequency()");
	}
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_speedMax
// 
// description : 	Extract real attribute values for speedMax acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_speedMax(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_speedMax(Tango::Attribute &attr) entering... "<< endl;
	
	attr.set_value(&speed_max);
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_speedMax
// 
// description : 	Write speedMax attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_speedMax(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_speedMax(Tango::WAttribute &attr) entering... "<< endl;
	
	attr.get_write_value(speed_max);
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_isPositiveRotation
// 
// description : 	Extract real attribute values for isPositiveRotation acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_isPositiveRotation(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_isPositiveRotation(Tango::Attribute &attr) entering... "<< endl;
	
	string plus  = motor_status.sens;
	
	if(plus == "+")
	{
		*attr_isPositiveRotation_read = true;
	}
	else if(plus == "-")
	{
		*attr_isPositiveRotation_read = false;
	}
	
	attr.set_value(attr_isPositiveRotation_read);
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_isPositiveRotation
// 
// description : 	Write isPositiveRotation attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_isPositiveRotation(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_isPositiveRotation(Tango::WAttribute &attr) entering... "<< endl;
	
	attr.get_write_value(attr_isPositiveRotation_write);
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_speed
// 
// description : 	Extract real attribute values for speed acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_speed(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_speed(Tango::Attribute &attr) entering... "<< endl;
	
	// speed is calculated in dev_state()
	attr.set_value(attr_speed_read);
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_speed
// 
// description : 	Write speed attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_speed(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_speed(Tango::WAttribute &attr) entering... "<< endl;
	
	double tmp;
	
	attr.get_write_value(tmp);
	
	if(tmp <= speed_max)
	{
		attr_speed_write = tmp;
	}
	else
	{
		Tango::Except::throw_exception(
			(const char*) "DATA_OUT_OF_RANGE",
			(const char*) " Speed must be lower than SpeedMax",
			(const char*) "CellCouette::write_speed()");    
	}
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_mode
// 
// description : 	Extract real attribute values for mode acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_mode(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_mode(Tango::Attribute &attr) entering... "<< endl;
	
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_mode
// 
// description : 	Write mode attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_mode(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_mode(Tango::WAttribute &attr) entering... "<< endl;
	
    Tango::DevUShort tmp;
	
    attr.get_write_value(tmp);
	
	if(tmp !=0 && tmp != 1)
	{
		Tango::Except::throw_exception(
			(const char*) "DATA_OUT_OF_RANGE",
			(const char*) " Mode must be equal to 0 or 1",
			(const char*) "CellCouette::write_mode()");
		
	}
	else
	{
		attr_mode_write = tmp;
	}
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_cellRadius
// 
// description : 	Extract real attribute values for cellRadius acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_cellRadius(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_cellRadius(Tango::Attribute &attr) entering... "<< endl;
	
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_cellRadius
// 
// description : 	Write cellRadius attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_cellRadius(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_cellRadius(Tango::WAttribute &attr) entering... "<< endl;
	
	attr.get_write_value(cell_radius);
	
	// update deformation
	if (  cell_gap != 0)
		deformation = (attr_halfAngularAmplitude_write) * (cell_radius + cell_gap) / cell_gap;
	
	
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_cellGap
// 
// description : 	Extract real attribute values for cellGap acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_cellGap(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_cellGap(Tango::Attribute &attr) entering... "<< endl;
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_cellGap
// 
// description : 	Write cellGap attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_cellGap(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_cellGap(Tango::WAttribute &attr) entering... "<< endl;
	
	attr.get_write_value(cell_gap);
	
	// update deformation
	if (  cell_gap != 0)
		deformation = (attr_halfAngularAmplitude_write) * (cell_radius + cell_gap) / cell_gap;
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::read_position
// 
// description : 	Extract real attribute values for position acquisition result.
//
//-----------------------------------------------------------------------------
void CellCouette::read_position(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CellCouette::read_position(Tango::Attribute &attr) entering... "<< endl;
	
	// get pos1 angle
	*attr_position_read = motor_status.position;
	
	// position unity is degree
	if(attr_positionUnity_write == 1)
	{
		*attr_position_read = (*attr_position_read) * STEP_DEG_RATIO;
	}
	
	attr.set_value(attr_position_read);
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouette::write_position
// 
// description : 	Write position attribute values to hardware.
//
//-----------------------------------------------------------------------------
void CellCouette::write_position(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CellCouette::write_position(Tango::WAttribute &attr) entering... "<< endl;
	
	string absolute_position;
	
	attr.get_write_value(attr_position_write);
	
	if(get_state() == Tango::STANDBY)
	{
		// step
		if(attr_positionUnity_write == 0)
		{
			absolute_position = XString<long>::convertToString(long(attr_position_write));
		}
		// degree
		else if(attr_positionUnity_write == 1)
		{
			int pos_in_deg = int(attr_position_write / STEP_DEG_RATIO);
			absolute_position = XString<int>::convertToString(pos_in_deg);
		}
		
		string cmd = "15GA " + absolute_position;
		
		_DEV_TRY(
			controller_response = write_read_on_serial_port(cmd),
			"move to new absolute position",
			"write_position");
	}
	else
	{
		Tango::Except::throw_exception(
			(const char*) "TANGO_DEVICE_ERROR",
			(const char*) "you can change the motor absolute position only if the motor is stopped",
			(const char*) "CellCouette::write_position()");
	}
}

//+------------------------------------------------------------------
/**
 *	method:	CellCouette::start
 *
 *	description:	method to execute "Start"
 *	start the motor motion according to  the operating mode selected (either CONTINUOUS or OSCILLATIONS)
 *	- CONTINOUS MODE:
 *	this is the write part of speed attribute  which define  the rotor speed once you push on start comand
 *	- OSCILLATIONS MODE:
 *	execute a sequence memorized in the controller (already programmed in the firmware)
 *
 *
 */
//+------------------------------------------------------------------
void CellCouette::start()
{
	DEBUG_STREAM << "CellCouette::start(): entering... !" << endl;
	
	//	Add your own code to control device here
	
	string cmd,value,sign;
	
    if(attr_mode_write == CONTINUOUS)
    {

		double speed_in_step;

		// Any speed prest must be sent in step / sec
		// therefore, according to the speed unity selected we convert the preset value in step / sec
		if(attr_speedUnity_write == STEP_PER_SECOND)
		{
			speed_in_step = attr_speed_write;
			// no converting
			DEBUG_STREAM <<" speed (step/s) = "<<speed_in_step<<endl;
		}
		else if(attr_speedUnity_write == HERTZ)
		{
			speed_in_step = attr_speed_write * STEP_PER_ROTATION;
			DEBUG_STREAM <<" speed (step/s) = "<<speed_in_step<<endl;
		}
		else if(attr_speedUnity_write == ROTATION_PER_MINUTE)
		{
			speed_in_step = attr_speed_write * STEP_PER_ROTATION / 60;
			DEBUG_STREAM <<" speed (step/s)  = "<<speed_in_step<<endl;
		}
		else if(attr_speedUnity_write == RADIAN_PER_SECOND)
		{
			speed_in_step = (attr_speed_write * STEP_PER_ROTATION) / 2 / Pi;
			DEBUG_STREAM <<" speed (step/s)  = "<<speed_in_step<<endl;      
		}
		else if(attr_speedUnity_write == SHEAR_RATE)
		{
			if(cell_gap != 0) {
				speed_in_step = attr_speed_write * STEP_PER_ROTATION / ( 2 * Pi * ((cell_radius + cell_gap) / cell_gap));
			}
			else {
				speed_in_step = 0;
			}
			//attr_speed_write = (attr_speed_write * 10000) / 360;
			DEBUG_STREAM <<" speed (step/s) = "<<speed_in_step<<endl;
		}		
		// the motor operates like:
		// real_speed = speed_write * resolution
		// the user enter real_speed and the resolution is known
		// therefore the speed value to write = real_speed / resolution
		speed_in_step = ::ceil(speed_in_step / resolution);
		
		if(attr_isPositiveRotation_write == true)
		{
			sign = "+";
		}
		else
		{
			sign = "-";
		}
		
		value = XString<long>::convertToString(long(speed_in_step));
		
		// GF +/-V: start the movement by accelerating or decelerating till V (step / sec)
		// possible error code:
		// 1 : paramter is incorrect
		cmd = "15GF " + sign + value;
		
		_DEV_TRY(
			controller_response = write_read_on_serial_port(cmd),
			"start motor in continuous mode",
			"start()");		
    }
    else if(attr_mode_write == OSCILLATIONS)
    {
		// SS n: execute sequence number "n" (3 digits max), here n = 2
		// possible error code:
		// 0 : parameter default, wrong format, parameter number incorrect
		// 1 : n > 100 ou n < 1
		// 3 : unknown sequency number 
		
		cmd = "15SS 2";
		
		_DEV_TRY(
			controller_response = write_read_on_serial_port(cmd),
			"start motor in oscillation mode",
			"start()");		
    }
    else
    {
		Tango::Except::throw_exception(
			(const char*) "TANGO_DEVICE_ERROR",
			(const char*) "unknown operating mode selected",
			(const char*) "CellCouette::start()");
    }
}

//+------------------------------------------------------------------
/**
 *	method:	CellCouette::stop
 *
 *	description:	method to execute "Stop"
 *	stop the  motor motion
 *
 *
 */
//+------------------------------------------------------------------
void CellCouette::stop()
{
	DEBUG_STREAM << "CellCouette::stop(): entering... !" << endl;
	
	//Add your own code to control device here
	
	std::string cmd;
	
	//  {
    if(attr_mode_write == CONTINUOUS)
    {
		// "GE": the current movement (direct or sequence) is decelerated till Vmin (WL) and finaly stopped
		// possibility to use "GS" which immediatly stop the movement
		// no error code
		cmd = "15GE";
		
		_DEV_TRY(
			controller_response = write_read_on_serial_port(cmd),
			"stop movement",
			"stop()");
				
		// set speed to 0 and 
		first_speed_measurement = true;
		*attr_speed_read = 0;
    }
    else if(attr_mode_write == OSCILLATIONS)
    {
		// "PO # n := X", set the variable number "n" to the value "X"
		// possible error code:
		// 0 : false variable number
		// 1 : "X" exceeds the maximum value
		
		cmd = "15PO #1 := 0";
		
		_DEV_TRY(
			controller_response = write_read_on_serial_port(cmd),
			"stop movement",
			"stop()");		
    }
	
}

//+------------------------------------------------------------------
/**
 *	method:	CellCouette::power_off
 *
 *	description:	method to execute "PowerOFF"
 *	cut the motor power
 *
 *
 */
//+------------------------------------------------------------------
void CellCouette::power_off()
{
	DEBUG_STREAM << "CellCouette::power_off(): entering... !" << endl;
	
	//	Add your own code to control device here
	
    // "GR", cut the motor power, current = 0 A
    // possible error code:
    // no special error code
    string cmd = "15GR";
	
    _DEV_TRY(
		controller_response = write_read_on_serial_port(cmd),
		"cut the motor power, current = 0 A",
		"power_off()");
}

//+------------------------------------------------------------------
/**
 *	method:	CellCouette::reset
 *
 *	description:	method to execute "Reset"
 *	the rotor returns to the default state (note that the encoder position is set to 0)
 *
 *
 */
//+------------------------------------------------------------------
void CellCouette::reset()
{
	DEBUG_STREAM << "CellCouette::reset(): entering... !" << endl;
	
	//	Add your own code to control device here
	
    // "MR", controller reset -> CPA = 0, current movement is interrupted, start automatic sequence
    // possible error code: no special error code
	
    string cmd = "15MR";
	
    _DEV_TRY(
		controller_response = write_read_on_serial_port(cmd),
		"reset controller",
		"CellCouette::reset()");
}

//+------------------------------------------------------------------
/**
 *	method:	CellCouette::send_command
 *
 *	description:	method to execute "SendCommand"
 *	send a specifc command to the specified controller.
 *	You must specify the controller adress in the string to send.
 *	adress list:
 *	15: motor controller
 *	03: temperature controller
 *	02:  torque controller
 *	
 *	ex: "15QX"
 *	where:
 *	15 :  the adress of the motor controller (2 bytes)
 *	QX:  is the command (N bytes)
 *
 * @param	argin	command to send 
 * @return	controller response
 *
 */
//+------------------------------------------------------------------
Tango::DevString CellCouette::send_command(Tango::DevString argin)
{
	//	POGO has generated a method core with argout allocation.
	//	If you would like to use a static reference without copying,
	//	See "TANGO Device Server Programmer's Manual"
	//		(chapter : Writing a TANGO DS / Exchanging data)
	//------------------------------------------------------------
	
    //	Add your own code to control device here
	std::string response;
	
    _DEV_TRY(
		response = write_read_on_serial_port(string(argin)),
		"write absoltue position to theta plus",
		"write_halfAngularAmplitude");
	
    DEBUG_STREAM<<"response to : "<< string(argin) <<" is: "<<response<<endl;
	
	return CORBA::string_dup(response.c_str());
}

//+------------------------------------------------------------------
/**
 *	method:	CellCouette::set_motor_param
 *
 *	description:	method to execute "SetMotorParam"
 *	Once you have written new values of speedMax, motorResolution, timeRamp attributes
 *	you must call setMotorParam(), thus new motor parameters are sent to the motorController
 *
 *
 */
//+------------------------------------------------------------------
void CellCouette::set_motor_param()
{
	DEBUG_STREAM << "CellCouette::set_motor_param(): entering... !" << endl;
	
	//	Add your own code to control device here
	
	string cmd,wt,wh,wn;

    long speed_max_ramp_in_step;

	// ---------- set SpeedMaxRamp -------------
	// unity management: convert to step / sec
	if(attr_speedUnity_write == STEP_PER_SECOND)
	{
		speed_max_ramp_in_step = speedMaxRamp;
		// no converting
		DEBUG_STREAM <<" speedMaxRamp (step/s) = "<<speedMaxRamp<<endl;
	}
	else if(attr_speedUnity_write == HERTZ)
	{
		speed_max_ramp_in_step = speedMaxRamp * STEP_PER_ROTATION;
		DEBUG_STREAM <<" speedMaxRamp  (step/s) = "<<speed_max_ramp_in_step<<endl;
	}
	else if(attr_speedUnity_write == ROTATION_PER_MINUTE)
	{
		speed_max_ramp_in_step = speedMaxRamp * 10000 / 60;
		DEBUG_STREAM <<" speedMaxRamp  (step/s) = "<<speed_max_ramp_in_step<<endl;
	}
	else if(attr_speedUnity_write == RADIAN_PER_SECOND)
	{
		speed_max_ramp_in_step = long((speedMaxRamp * STEP_PER_ROTATION) / 2 / Pi);
		DEBUG_STREAM <<" speedMaxRamp (step/s) = "<<speed_max_ramp_in_step<<endl;      
	}
	else if(attr_speedUnity_write == SHEAR_RATE)
	{
		if(cell_gap != 0) {
			speed_max_ramp_in_step = long( speedMaxRamp * STEP_PER_ROTATION / ( 2 * Pi * ((cell_radius + cell_gap) / cell_gap)));
		}
		else {
			speed_max_ramp_in_step = 0;
		}
		//speedMaxRamp = (speedMaxRamp * 10000) / 360;
		DEBUG_STREAM <<" speedMaxRamp (step/s) = "<<speed_max_ramp_in_step<<endl;
	}
	
	// construct WH command
	wh = XString<long>::convertToString(long(speed_max_ramp_in_step));
	cmd = "15WH " + wh;
	
	_DEV_TRY(
		controller_response = write_read_on_serial_port(cmd),
		"send speedMaxRamp (WH)",
		"send_movement_law_data()");
	
	// ---------- set TimeRamp -----------------
	long tmp = long(timeRamp * 1000);
	wt = XString<long>::convertToString(long(tmp));
	cmd = "15WT " + wt;
	
	_DEV_TRY(
		controller_response = write_read_on_serial_port(cmd),
		"send TimeRamp (WT)",
		"send_movement_law_data()");
	
	// --------- set MotorResolution -----------
	// construct WN command
	wn = XString<unsigned short>::convertToString(resolution);
	cmd = "15WN " + wn;
	
	_DEV_TRY(
		controller_response = write_read_on_serial_port(cmd),
		"send MotorResolution (WN)",
		"send_movement_law_data()");
}

//+------------------------------------------------------------------
/**
 *	method:	CellCouette::get_motor_param
 *
 *	description:	method to execute "GetMotorParam"
 *	get motor parameters
 *
 * @return	motor parameters
 *
 */
//+------------------------------------------------------------------
Tango::DevString CellCouette::get_motor_param()
{
	//	POGO has generated a method core with argout allocation.
	//	If you would like to use a static reference without copying,
	//	See "TANGO Device Server Programmer's Manual"
	//		(chapter : Writing a TANGO DS / Exchanging data)
	//------------------------------------------------------------
	DEBUG_STREAM << "CellCouette::get_motor_param(): entering... !" << endl;
	
	//	Add your own code to control device here
	
	string cmd = "15QL";
	
	_DEV_TRY(
		controller_response = write_read_on_serial_port(cmd),
		"get motor parameters (WH,WL,WT etc...)",
		"get_motor_param()");
	
	
	return CORBA::string_dup(controller_response.c_str());
}

//+------------------------------------------------------------------
/**
*	method:	CellCouette::write_read_on_serial_port()
*/
//+------------------------------------------------------------------
std::string CellCouette::write_read_on_serial_port(std::string cmd)
{
    INFO_STREAM<<"write_read_on_serial_port() entering..."<<endl;
    INFO_STREAM<<"command brute= "<<cmd <<endl;
	
    std::string dev_addr,data;
    std::string response;
	
    if(serial_proxy == 0)
    {
        create_serial_proxy();
    }
	
    // we know that the cmd parameter includes the adress of the device to control (2 first digits)
    dev_addr = cmd.substr(0,2);
    int length = cmd.length();
    cmd = cmd.substr(2,length - 2);
	
    // sleep for 500 ms
    //   omni_thread::sleep(0, SLEEPING_TIME);
	
    //- create a ascii frame according to the specified controller
    _DEV_TRY(
		cmd = create_controller_cmd(cmd,dev_addr),
		"create an ascii frame according to the specified controller",
		"CellCouette::write_read_on_serial_port()");
	
    INFO_STREAM<< "cmd to send to the controller = "<< cmd << endl;
	
    if(dev_addr == "15")
    {
		Tango::DevVarCharArray * stx = new Tango::DevVarCharArray;
		Tango::DevVarCharArray * etx = new Tango::DevVarCharArray;
		
		stx -> length(1);
		(*stx)[0] = 0x02;
		
		etx -> length(1);
		(*etx)[0] = 0x03;
		
		//- send stx character
		_DEV_TRY_REACTION(
			serial_proxy->command_in("DevSerWriteChar",stx),
			"send stx character",
			"CellCouette::write_read_on_serial_port()",
			if(serial_proxy)
			{
				delete serial_proxy;
				serial_proxy = 0;
			});
			
			
			//- send command to the controller
			long nb_written_char=0;
			_DEV_TRY_REACTION(
				serial_proxy->command_inout("DevSerWriteString",cmd.c_str(),nb_written_char),
				"send command to the motor controller",
				"CellCouette::write_read_on_serial_port()",
				if(serial_proxy)
				{
					delete serial_proxy;
					serial_proxy = 0;
				});
				
				//- send etx character
				_DEV_TRY_REACTION(
					serial_proxy->command_in("DevSerWriteChar",etx),
					"send etx character",
					"CellCouette::write_read_on_serial_port()",
					if(serial_proxy)
					{
						delete serial_proxy;
						serial_proxy = 0;
					});
					
					//delete stx;
					//delete etx;
    }
    else if(dev_addr == "03")
    {
		Tango::DevVarCharArray * eot = new Tango::DevVarCharArray;
		Tango::DevVarCharArray * enq = new Tango::DevVarCharArray;
		
		eot -> length(1);
		(*eot)[0] = 0x04;
		
		enq -> length(1);
		(*enq)[0] = 0x05;
		
		//- send stx character
		_DEV_TRY(
			serial_proxy->command_in("DevSerWriteChar",eot),
			"send eot character",
			"CellCouette::write_read_on_serial_port()");
		
		//- send command to the controller
		long nb_written_char=0;
		_DEV_TRY_REACTION(
			serial_proxy->command_inout("DevSerWriteString",cmd.c_str(),nb_written_char),
			"send command to the temperature controller",
			"CellCouette::write_read_on_serial_port()",
			if(serial_proxy)
			{
				delete serial_proxy;
				serial_proxy = 0;
			});
			
			//- send etx character
			_DEV_TRY_REACTION(
				serial_proxy->command_in("DevSerWriteChar",enq),
				"send enq character",
				"CellCouette::write_read_on_serial_port()",
				if(serial_proxy)
				{
					delete serial_proxy;
					serial_proxy = 0;
				});
				
				//delete eot;
				//delete enq;
    }
	
    else if(dev_addr == "02")
    {
		//- send command to the controller
		long nb_written_char=0;
		_DEV_TRY_REACTION(
			serial_proxy->command_inout("DevSerWriteString",cmd.c_str(),nb_written_char),
			"send command to the torque controller",
			"CellCouette::write_read_on_serial_port()",
			if(serial_proxy)
			{
				delete serial_proxy;
				serial_proxy = 0;
			});
    }
	
    INFO_STREAM<< "command sent to the controller !"<< endl;
	
    //- read response
	// before reading the response, the controllers need time ...
	// this break is very important to ensure a good communication.
	//  we have noted that 500 ms is a good value but it's an experimental observation
	omni_thread::sleep(0, SLEEPING_TIME);
	
	_DEV_TRY_REACTION(
        serial_proxy->command_out("DevSerReadRaw",response),
        "read controller's response",
        "CellCouette::write_read_on_serial_port()",
        if(serial_proxy)
        {
			delete serial_proxy;
			serial_proxy = 0;
        });
		
        INFO_STREAM<<"controller response before extracting data:	"<<response<<endl;
		
        if(response.empty()==false)
        {
            data = extract_data_from_controller_response(response,dev_addr);
        }
        else
        {
            INFO_STREAM<< " reponse to the command is empty !"<< endl;
            data = "no response";
        }
		
		
		INFO_STREAM<< "data extracted: "<<data<< "\n"<< endl;
		return data;
}

//+------------------------------------------------------------------
/**
*	method:	extract_data_from_controller_response()
*
*	description:	extract data part from the controller response
*/
//+------------------------------------------------------------------
std::string CellCouette::extract_data_from_controller_response(std::string response,string dev_addr)
{
	DEBUG_STREAM<<"extract_data_from_controller_response() entering..."<<endl;
	
	// data part of controller response
	string data_motor, data_temperature, data_torque;
	unsigned char ack_byte = 0x00;
	int dev_ad=0,size=0;
	
	size = response.size();
	
	dev_ad = XString<int>::convertFromString(dev_addr);
	
    switch(dev_ad)
    {
	case MOTOR:
		INFO_STREAM<<"MOTOR controller concerned:"<<endl;
		
		// the first character of the responses is an acknowlegment byte (ACK or NACK or BEL)
		// extract first character from the response 
		ack_byte = response[0];
		
		DEBUG_STREAM <<"ack_byte= "<< ack_byte <<endl;
		
		// what is the nature of the ack_byte ?
		if(ack_byte == 0x06)
		{
			INFO_STREAM<<" ACK returned[good interpretation of the current message]"<<endl;
			
			if(size > 1)        // AB: Renforcer le tests sur le nombre mini de caract�res n�cessaire
			{
                // the response is not only an ACK byte, but some data are also include into the response
                // we want extract the data (bytes to removes = 10 = : format = acq_byte(1),stx(1),nc(3),addr(2),data(N bytes),CS(2),etx(1)
                data_motor = response.substr(7,size - 10);
			}
			else
			{
                INFO_STREAM <<"ACK byte received but no message more from controller"<<endl;
                data_motor = "no response"; 
			}
		}
		else if(ack_byte == 0x07)
		{
			INFO_STREAM<<"BEL returned [good interpretation of the current message but the precedent was bad]"<<endl;
			
			if(size > 1)             // AB: Renforcer le tests sur le nombre mini de caract�res n�cessaire
			{
                // the response is not only an ACK byte, but some data are also include into the response
                // we want extract the data (bytes to removes = 10 = : format = acq_byte(1),stx(1),nc(3),addr(2),data(N bytes),CS(2),etx(1)
                data_motor = response.substr(7,size - 10);
			}
			else
			{
                INFO_STREAM <<"BEL byte received but no message more from controller"<<endl;
                data_motor = "no response"; 
			}
		}
		else if(ack_byte == 0x15)
		{
			INFO_STREAM<<"NACK returned[bad interpretation of the current message, or timeout(70 ms) exceeded]"<<endl;
			data_motor = "no response"; 
		}
		else
		{
			INFO_STREAM<<"wrong syntax response"<<endl;
			data_motor = "no response"; 
		}
		
		return data_motor;
		
	case TORQUE_SENSOR:
		INFO_STREAM<<"TORQUE controller concerned:"<<endl;
		
		// No error information is sent if there is a problem0. We have no means to know if the command sent has been well
		// interpreted by the controller. So we assume that the response is every time OK
		INFO_STREAM<<"torque response size= "<< size<< endl;
		
		// we have noted that a right response is a string with 14 characters
		if(size == 14)
		{
            data_torque = response;
		}
		else
		{
			data_torque = "no response";              
		}
		
		return data_torque;
		
	case TEMPERATURE_SENSOR:
		INFO_STREAM<<"TEMPERATURE controller concerned:"<<endl;
		
		// possible responses:
		// - no response:
		// - character EOT = 0x04 (terminate the data link)
		// - the frame = stx/identifier/data/etx/bcc
		//   stx = start message (0x02) -> 1 byte
		//   identifier = command       -> 2 bytes
		//   data = data to extract     -> 6 bytes     --> controller message size = 11 bytes;
		//   etx = end message (0x03)   -> 1 byte
		//   bbc = checksum             -> 1 byte
		//   at the moment we consider that the message sent by the controller is good, therefore we don't calculate the bbc byte 
		
		ack_byte = response[0];    // AB: Renforcer le tests sur le nombre mini de caract�res n�cessaire
		
		if((ack_byte == 0x00) || (ack_byte == 0x04)) // no response or EOT
		{
			INFO_STREAM <<"Data link terminated [syntax error message or timeout or link volontary stopped by the controller "<<endl;
			
			data_temperature = "no response";
		}
		else if(ack_byte == 0x02) //stx
		{
			INFO_STREAM <<"good interpretation of the last message sent by the host "<<endl;
			data_temperature = response.substr(3,6);           // AB: Renforcer le tests sur le nombre mini de caract�res n�cessaire
			
			if(size>1)
			{
                // the response is not only an ACK byte, some data are also include into the response
                // we want extract the data part which start at index 3 for 6 digits
                data_temperature = response.substr(3,6);     // AB: Renforcer le tests sur le nombre mini de caract�res n�cessaire
			}
			else
			{
                data_temperature = "no response";
			}
		}
		return data_temperature;
        
	default:
        {
            INFO_STREAM<<"you have tried to communicate with a unknown controller"<<endl;
            return "no response";
        }
    }
}
//+------------------------------------------------------------------
/**
*	method:	CellCouette::create_controller_cmd()
*
*	description:	method to execute "write_read"
* argin:  - cmd = host command
*         - dev_addr = controller address
*/
//+------------------------------------------------------------------
std::string CellCouette::create_controller_cmd(string cmd,string dev_addr)
{
    DEBUG_STREAM<<"create_controller_cmd() entering..."<<endl;
    DEBUG_STREAM<<"cmd = "<<cmd<<endl;
    DEBUG_STREAM<<"dev_addr = "<<dev_addr<<endl;
	
    unsigned int nc=0,dev_ad=0;
    ostringstream os,temp;
    std::string CS;
	
    dev_ad = XString<int>::convertFromString(dev_addr);
	
    switch(dev_ad)
    {
	case MOTOR:
		
		DEBUG_STREAM<<"MOTOR CONTROLLER REQUEST:"<<endl;
		
		// the cmd to send to the motor controller should respect a specific format as:
		// stx/nc/addr/cmd/CS/etx
		
		// insert nc shield into os
		nc = cmd.size() + dev_addr.size();
		DEBUG_STREAM<<"nc= "<<nc<<endl;
		
		if(nc < 10)
		{
			// 1 digit
			os<<0<<0<<nc;
		}
		else if(nc < 100)
		{
			// 2 digits
			os<<0<<nc;
		}
		else if(nc < 1000)
		{
			// 3 digits
			os<<nc;
		}
		
		// insert addr into os
		os<<dev_addr;
		
		// insert cmd into os
		os<<cmd;
		
		// insert CS into os
		CS = checksum_calculation(dev_addr + cmd);
		os<<CS;
		
		// format to string
		cmd = os.str();
		
        break;
		
	case TORQUE_SENSOR:
		DEBUG_STREAM<<"TORQUE CONTROLLER REQUEST:"<<endl;
		
        // we just need to read weigh measurement.
        cmd = "N02TA*";
		
        break;
		
	case TEMPERATURE_SENSOR:
		
		DEBUG_STREAM<<"TEMPERATURE CONTROLLER REQUEST:"<<endl;
		
		// the cmd to send to the temperature controller should respect the following format:
		// eot/addr/cmd/enq
		
		// insert addr into os
		os<<dev_addr;
		
		// insert cmd into os
		os<<cmd;
		
		// format to string
		cmd = os.str();
        break;
		
	default:
		DEBUG_STREAM<<"you try to communicate with a unknown controller"<<endl;
    }
    return cmd;
}

//+------------------------------------------------------------------
/**
*	method:	CellCouette::checksum_calculation
*
*	description:	return the checksum
*/
//+------------------------------------------------------------------
std::string CellCouette::checksum_calculation(std::string cmd)
{
    DEBUG_STREAM<<"checksum_calculation() entering..."<<endl;
	
    unsigned short checksum = 0;
    std::string CS;
    ostringstream os;
	
    unsigned int length = cmd.length();
    
    for(size_t i=0; i<length; i++)
    {
		// hexadecimal sum of all characters
		checksum += cmd[i];
    }
	
    // format in hexa type
    os << hex << checksum;
	
    // to get the checksum modulo 256 in string type 
    CS = os.str();
    int size = CS.size();
    CS = CS.substr(size - 2,2);
	
    // we need upper characters...
    if(CS[0]=='a' || CS[0]=='b' || CS[0]=='c' || CS[0]=='d' || CS[0]=='e' || CS[0]=='f')
    {
		CS[0] = CS[0] - 0x20;
    }
	
    if(CS[1]=='a' || CS[1]=='b' || CS[1]=='c' || CS[1]=='d' || CS[1]=='e' || CS[1]=='f')
    {
		CS[1] = CS[1] - 0x20;
    }    
    
    DEBUG_STREAM<<"CheckSum = "<< CS << endl;
    
    return CS;
}

//+------------------------------------------------------------------
/*
*	create_serial_proxy()
*/
//+------------------------------------------------------------------
void CellCouette::create_serial_proxy()
{
    DEBUG_STREAM << "CellCouette::create_serial_proxy: entering... !" << endl;
    
    //	Add your own code to control device here
    try
    {
        serial_proxy = new Tango::DeviceProxyHelper(serialProxyName,this);
        
        if(serial_proxy==0)
        {
            throw std::bad_alloc();
        }
    }
    catch(std::bad_alloc)
    {
        Tango::Except::throw_exception
            (
            (const char*) "OUT_OF_MEMORY",
            (const char*) "can't create serial device proxy",
            (const char*) "CellCouette::create_serial_proxy()"
            );        
    }
}


//+------------------------------------------------------------------
/**
*	method:	CellCouette::dev_state
*
*	description:	method to execute "State"
*	This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
*
* @return	State Code
*
*/
//+------------------------------------------------------------------
Tango::DevState CellCouette::dev_state()
{
	Tango::DevState	argout = DeviceImpl::dev_state();
	DEBUG_STREAM << "CellCouette::dev_state(): entering... !" << endl;
	
	//	Add your own code to control device here
	if(serial_proxy == 0)
	{
		device_status += "no more communication with serial line\n";
		set_status(device_status.c_str());
		return Tango::FAULT;
	}
	
	device_status   = " ////////// STATUS //////////\n";
	
	if(attr_mode_write == CONTINUOUS)
	{
		device_status += "OPERATING mode: CONTINUOUS\n";
	}
	else if(attr_mode_write == OSCILLATIONS)
	{
		device_status += "OPERATING mode: OSCILLATIONS\n"; 
	}
	else
	{
		device_status += "OPERATING mode: WRONG\n";
	}
	
	if(attr_positionUnity_write == STEP)
	{
		device_status += "POSITION UNITY: STEPS\n";
	}
	else if(attr_positionUnity_write == DEGREE)
	{
		device_status += "POSITION UNITY: DEGREE\n";
	}
	else
	{
		device_status += "POSITION UNITY: WRONG\n"; 
	}
	
	if(attr_speedUnity_write == STEP_PER_SECOND)
	{
		device_status += "SPEED UNITY: STEP PER SECOND\n";
	}
	else if(attr_speedUnity_write == HERTZ)
	{
		device_status += "SPEED UNITY: HERTZ\n";    
	}
	else if(attr_speedUnity_write == ROTATION_PER_MINUTE)
	{
		device_status += "SPEED UNITY: ROTATION_PER_MINUTE\n";
	}
	else if(attr_speedUnity_write == RADIAN_PER_SECOND)
	{
		device_status += "SPEED UNITY: RADIAN_PER_SECOND\n";
	}
	else if(attr_speedUnity_write == SHEAR_RATE)
	{
		device_status += "SPEED UNITY: SHEAR_RATE\n";
	}
	else
	{
		device_status += "SPEED UNITY: WRONG\n";
	}
	
	_DEV_TRY(
		controller_response = write_read_on_serial_port("15QD"),
		"get motor status",
		"get_motor_status");
	
    if(controller_response != "no response")
    {
		
		// get time in sec
		time(&ltime);
		long time = ltime;
		
		int length = controller_response.length();
		DEBUG_STREAM<<"response size = "<<length<<endl;
		
		// response to QD cmd is like:
		// "ED sequency phasis sens nature position input output control seqn+1 error_code"
		// 45 bytes max, with spaces between all fields
		
		// AB: Renforcer le tests le resultat des finds
		
		int index_sequency = controller_response.find(' ',0) + 1;
		int index_phasis = controller_response.find(' ',index_sequency) + 1;
		int index_sens = controller_response.find(' ',index_phasis) + 1;
		int index_nature = controller_response.find(' ',index_sens) + 1;
		int index_position = controller_response.find(' ',index_nature) + 1;
		int index_input = controller_response.find(' ',index_position) + 1;
		int index_output = controller_response.find(' ',index_input) + 1;
		int index_control = controller_response.find(' ',index_output) + 1;
		int index_sequency_n_plus_1 = controller_response.find(' ',index_control) + 1;
		int index_error_code = controller_response.find(' ',index_sequency_n_plus_1) + 1;
		
		string sequency = controller_response.substr(index_sequency,index_phasis - index_sequency - 1);
		DEBUG_STREAM<<"sequency = "<<sequency<<endl;
		
		string phasis = controller_response.substr(index_phasis,index_sens - index_phasis - 1);
		DEBUG_STREAM<<"phasis = "<<phasis<<endl;
		
		string sens = controller_response.substr(index_sens,index_nature - index_sens - 1);
		DEBUG_STREAM<<"sens = "<<sens<<endl;
		
		string nature = controller_response.substr(index_nature,index_position - index_nature - 1);
		DEBUG_STREAM<<"nature = "<<nature<<endl;
		
		string position = controller_response.substr(index_position,index_input - index_position - 1);
		DEBUG_STREAM<<"position = "<<position<<endl;
		
		// update speed
		motor_status.position = XString<long>::convertFromString(position);
		
		// at the first position measurement we don't update speed value because we haven't any measurement reference
		if(first_speed_measurement)
		{
			old_time = time;
			old_position = motor_status.position;
			first_speed_measurement = false;
		}
		else
		{
			CellCouette::speed_calculation(time, motor_status.position);
		}
		
		string input = controller_response.substr(index_input,index_output - index_input - 1);
		DEBUG_STREAM<<"input = "<<input<<endl;
		
		string output = controller_response.substr(index_output,index_control - index_output - 1);
		DEBUG_STREAM<<"output = "<<output<<endl;
		
		string control = controller_response.substr(index_control,index_sequency_n_plus_1 - index_control - 1);
		DEBUG_STREAM<<"control = "<<control<<endl;
		
		string sequency_n_plus_1 = controller_response.substr(index_sequency_n_plus_1,index_error_code - index_sequency_n_plus_1 - 1);
		DEBUG_STREAM<<"sequency_n_plus_1 = "<<sequency_n_plus_1<<endl;
		
		string error_code = controller_response.substr(index_error_code,length - index_error_code);
		DEBUG_STREAM<<"error_code = "<<error_code<<endl;
		
		motor_status.control = control;
		motor_status.error_code = error_code;
		motor_status.input = input;
		motor_status.nature = nature;
		motor_status.output = output;
		motor_status.phasis = XString<int>::convertFromString(phasis);
		motor_status.sens = sens;
		motor_status.sequency = XString<int>::convertFromString(sequency);
		motor_status.sequency_n_plus_1 = XString<int>::convertFromString(sequency_n_plus_1);
		
		
		if(nature == "NI") // is NV in microsympas the documentation p.62
		{
			device_status += "rotor is moving with a constant speed\n";
			set_status(device_status.c_str());
			set_state(Tango::MOVING);
		}
		
		else if(nature == "XX")
		{
			device_status += "motor is stopped\n";
			set_status(device_status.c_str());
			set_state(Tango::STANDBY);
		}
		else
		{
			device_status += "motor status unknown\n";
			set_status(device_status.c_str());
			set_state(Tango::UNKNOWN);
		}
   }
   else
   {
	   device_status += "no response to the state request\n";
	   set_status(device_status.c_str());
	   set_state(Tango::UNKNOWN);
   }
   return argout;
}

//+------------------------------------------------------------------
/**
 *	method:	CellCouette::get_motor_state
 *
 *	description:	method to execute "GetMotorState"
 *	return a message which inform you on the motor state
 *
 * @return	motor state
 *
 */
//+------------------------------------------------------------------
Tango::DevString CellCouette::get_motor_state()
{
	//	POGO has generated a method core with argout allocation.
	//	If you would like to use a static reference without copying,
	//	See "TANGO Device Server Programmer's Manual"
	//		(chapter : Writing a TANGO DS / Exchanging data)
	//------------------------------------------------------------
	DEBUG_STREAM << "CellCouette::get_motor_state(): entering... !" << endl;
	
	//	Add your own code to control device here
	
	string motor_state,description;
	
    _DEV_TRY(
		motor_state = write_read_on_serial_port("15QX"),
		"get motor state",
		"get_motor_state()");
	
	long index_code = motor_state.find("EE ",0);
	
	if(index_code != -1)
	{
		// AB tester le nb de caract�re avant de faire le substr
		string error_code = motor_state.substr(index_code + 3,1);
		
		if(error_code == "N")
		{
			description = "normal working";
		}
		else if(error_code == "A")
		{
			description = "error level 1, code A : command disabled because of the actual motor state";
		}
		else if(error_code == "B")
		{
			description = "error level 1, code B : but�e detected => motor has been stopped";
		}
		else if(error_code == "C")
		{
			description = "error level 1, code C : unknown command [syntax error]";
		}
		else if(error_code == "D")
		{
			description = "error level 1, code D : coordination phasis error";
			
		}
		else if(error_code == "E")
		{
			description = "error level 1, code E : default on logic input";
		}
		else if(error_code == "I")
		{
			description = "error level 1, code I : impossible to change the current motor by programmming a new value (cf GI commmand)";
			return CORBA::string_dup(description.c_str());
		}
		else if(error_code == "M")
		{
			description = "error level 1, code M : Memory reinitialization";
			
		}
		else if(error_code == "S")
		{
			description = "error level 1, code S : phasis n�255 reached !";
		}
		else if(error_code == "W")
		{
			description = "error level 1, code W : power supply default";
		}
		else if(error_code == "X")
		{
			description = "error level 1, code X : hardware default";
		}
		else if(error_code == "0")
		{
			description = "error level 2, code 0 : command parameter error";
		}
		else if(error_code == "1")
		{
			description = "error level 2, code 1 : first parameter out of limit or no defined sequency";
		}
		else if(error_code == "2")
		{
			description = "error level 2, code 2 : second parameter out of limit or no defined sequency";
		}
		else if(error_code == "3")
		{
			description = "error level 2, code 3 : the sequency in the command does not exist";
		}
		else if(error_code == "4")
		{
			description = "error level 2, code 4 : unknown phasis or already created sequency";
		}
		else if(error_code == "5")
		{
			description = "error level 2, code 5 : the number of available phasis is to low to create a new sequency";
		}
	}
	else
	{
		DEBUG_STREAM <<"impossible to read the current motor state[response error syntax]"<<endl;
		description = "impossible to read the current motor state[response error syntax]";
	}
	
	// return description
	return CORBA::string_dup(description.c_str());
	
	
}

//+------------------------------------------------------------------
/**
*	method:	CellCouette::speed_calculation
*
*  description: speed calculation
*/
//+------------------------------------------------------------------
void CellCouette::speed_calculation(long time,long position)
{
	long speed=0;
	
	// calculate speed in pas / sec
	if ( (time - old_time)!=0 )
	{
		speed = (position - old_position)/(time - old_time);
    this->raw_speed = speed;
	}
	DEBUG_STREAM <<" current position = "<< position <<endl;
	DEBUG_STREAM <<" current time = "<< time <<endl;
	DEBUG_STREAM <<" old_position = "<< old_position <<endl;
	DEBUG_STREAM <<" old_time = "<< old_time <<endl;
	INFO_STREAM <<"speed = (position - old_position)/(time - old_time) = "<< (position - old_position)/(time - old_time) <<endl;
	
	// update old_position & old_time, useful for the next speed calculation
	old_position = position;
	old_time = time;
	
	if(attr_speedUnity_write == STEP_PER_SECOND)
	{
		*attr_speed_read = speed;
		DEBUG_STREAM <<" speed calculated(step/s) = "<<*attr_speed_read<<endl;
	}
	else if(attr_speedUnity_write == HERTZ)
	{
		*attr_speed_read = speed / STEP_PER_ROTATION;
		DEBUG_STREAM <<" speed calculated(Hz) = "<<*attr_speed_read<<endl;
	}
	else if(attr_speedUnity_write == ROTATION_PER_MINUTE)
	{
		*attr_speed_read = (speed / STEP_PER_ROTATION) * 60;
		DEBUG_STREAM <<" speed calculated(tour/min)  = "<<*attr_speed_read<<endl;
	}
	else if(attr_speedUnity_write == RADIAN_PER_SECOND)
	{
		*attr_speed_read = (speed / STEP_PER_ROTATION) * 2 * Pi;
		DEBUG_STREAM <<" speed calculated(rad/s)  = "<<*attr_speed_read<<endl;      
	}
	else if(attr_speedUnity_write == SHEAR_RATE)
	{
		if(cell_gap != 0) {
			*attr_speed_read = speed * ((2 * Pi / STEP_PER_ROTATION) * ((cell_radius + cell_gap) / cell_gap));
		}
		else {
			*attr_speed_read = 0;
		}
		//*attr_speed_read = (speed / 10000) * 360;
		DEBUG_STREAM <<" speed calculated(shear rate) = "<<*attr_speed_read<<endl;
	}
}


//+------------------------------------------------------------------
/**
*	method:	CellCouette::update speed_setpoint_in_hertz
*
*  description: calculate speed in hertz
*/
//+------------------------------------------------------------------
void  CellCouette::update_speed_setpoint_in_hertz()
{
DEBUG_STREAM <<" update_speed_setpoint_in_hertz "<<endl;

switch(attr_speedUnity_write)
			{
			case STEP_PER_SECOND:
						speed_setpoint_in_hertz = attr_speed_write / STEP_PER_ROTATION;
						break;
			case HERTZ:
						speed_setpoint_in_hertz = attr_speed_write;
						break;					
			case ROTATION_PER_MINUTE:
						speed_setpoint_in_hertz = attr_speed_write / 60;
						break;
			case RADIAN_PER_SECOND:
						speed_setpoint_in_hertz  = attr_speed_write / (2*Pi);
						break;
					case SHEAR_RATE:
         {
			       double r1_carre=0.;
			       double r2_carre=0.;
							r1_carre= (cell_radius + cell_gap)* (cell_radius + cell_gap);
							r2_carre = cell_radius *cell_radius;

						if((r1_carre + r2_carre) != 0) {
							speed_setpoint_in_hertz  = attr_speed_write * (r1_carre-r2_carre)/((r1_carre + r2_carre) *2*Pi);
						}
						break;
	        }
}
}


}	//	namespace
