#!/usr/bin/env python
#
# ------------------------------------------------------------------------------
#
# file : callSerialLineForCellCouette.py
#
# description :
#
#
# project : Swing
#
#
# $Author:	$
#
# $Revision: $
# ------------------------------------------------------------------------------
import sys
import string
import time
from PyTango import *

def send_message_to_serial(dev, message, read_required):

    try:
		    # Vider le buffer E/S
		    #dev.command_inout("DevSerFlush", 2)
		    #print "DevSerFlush"

		    # Envoyer le carat�re de debut de string
		    charStartMessage = []
		    charStartMessage.append(2)
		    dev.command_inout("DevSerWriteChar",charStartMessage)

		    # Envoyer la commande
		    nb_char_written = dev.command_inout("DevSerWriteString",message)
		    #print "DevSerWriteString return value = " + str(nb_char_written)

		    # Envoyer le carat�re de fin de string
		    charEndMessage = []
		    charEndMessage.append(3)
		    dev.command_inout("DevSerWriteChar",charEndMessage)

		    if read_required:
			    time.sleep(0.5)
			    #print "before response"
			    response = dev.command_inout("DevSerReadRaw") # try DevSerReadLine
			    #print "response received: " + response
			    return response
    except Exception, ex:
		    print " ======> An Exception has been received in send_message_to_serial during : " + message, ex

    return "NO_WAITED_REPONSE_FOR " + message

def main():

  # Recuperation du device proxy
  try:
    dev = DeviceProxy("I11-C-C00/CA/RS232_8.1-COM5")

    waiting_time = 0.5
    req_num = 0

    # execution de la sequence n fois de suite
    for i in range(10):
    #if 1:
	# Read motor status
	#response = send_message_to_serial(dev,"00415QDFB",1)
	#req_num = req_num + 1
	#print str(req_num) + " - Read motor status : " + str(response)
	#time.sleep(waiting_time)

	# Read motor status
	#response = send_message_to_serial(dev,"00415QX0F",1)
	#req_num = req_num + 1
	#print str(req_num) + " - Read motor state : " + str(response)
	#time.sleep(waiting_time)

	# Vider le buffer E/S
	dev.command_inout("DevSerFlush", 2)
	print "DevSerFlush"

	# Read motor status
	response = send_message_to_serial(dev,"00415QL03",1)
	#req_num = req_num + 1
	print str(req_num) + " - Read motor parameters : " + str(response)
	#ime.sleep(waiting_time)
	time.sleep(0.1)
	# Vider le buffer E/S
	dev.command_inout("DevSerFlush", 2)
	print "DevSerFlush"

	# Read torque
	nb_char_written = dev.command_inout("DevSerWriteString","N02TA*")
	#ime.sleep(2)
	response = dev.command_inout("DevSerReadRaw")
	print str(req_num) + " - Read weight : " + str(response)
	time.sleep(2)

  except Exception, ex:
    print " ======> An Exception has been received : ", ex

if __name__ == '__main__':
		main()
		
