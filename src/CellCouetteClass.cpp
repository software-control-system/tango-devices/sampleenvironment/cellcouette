static const char *RcsId     = "$Header: /users/chaize/newsvn/cvsroot/Instrumentation/CellCouette/src/CellCouetteClass.cpp,v 1.16 2010-03-26 09:16:38 vince_soleil Exp $";
static const char *TagName   = "$Name: not supported by cvs2svn $";
static const char *HttpServer= "http://www.esrf.fr/computing/cs/tango/tango_doc/ds_doc/";
//+=============================================================================
//
// file :        CellCouetteClass.cpp
//
// description : C++ source for the CellCouetteClass. A singleton
//               class derived from DeviceClass. It implements the
//               command list and all properties and methods required
//               by the CellCouette once per process.
//
// project :     TANGO Device Server
//
// $Author: vince_soleil $
//
// $Revision: 1.16 $
//
// $Log: not supported by cvs2svn $
// Revision 1.15  2009/03/10 15:06:55  buteau
// en cours
//
// Revision 1.14  2008/12/08 14:06:57  jean_coquet
// changed torque calculation
// added attribute viscosity
// changed stress calculation
//
// Revision 1.13  2008/02/20 15:13:52  pierrejoseph
// stain has been renamed in stress (requested by user) + polling values modifications
//
// Revision 1.12  2008/02/01 17:08:32  sebleport
// - halfAngularAmplitude is no more a polled attribute. finallly we don't read this value on the hardware.
// - some bugs about unity conversions are corrected
//
// Revision 1.11  2008/02/01 11:49:56  sebleport
// - pollinn values have been modified
// temperature: 30000 ms
// weight: 1000 ms
// halfAngularAmplitude: 30000ms
//
// Revision 1.10  2008/01/31 10:13:12  sebleport
// - no message
//
// Revision 1.9  2008/01/29 16:53:15  sebleport
// - halfAngular, state, temperature, weight attributes are polled.
// - the device server well work with ATK PANEL client
//
// Revision 1.8  2008/01/29 14:01:49  sebleport
// - documenation modofied
// - resolution choice is now 1 or 64
//
// Revision 1.7  2008/01/25 18:29:28  sebleport
// - add motorResolution attiributes:
// - timeRamp & speedMaxRamp are now R/W.
//
// Revision 1.6  2008/01/24 18:35:25  sebleport
// - sendMovementLawdata() replaced by setMotorParam()
// - descrtiption command / attributes have changed
// - speed calculation is done in dev_state() by using a polling method
//
//
// copyleft :   European Synchrotron Radiation Facility
//              BP 220, Grenoble 38043
//              FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>

#include <CellCouette.h>
#include <CellCouetteClass.h>


//+----------------------------------------------------------------------------
/**
 *	Create CellCouetteClass singleton and return it in a C function for Python usage
 */
//+----------------------------------------------------------------------------
extern "C" {
#ifdef WIN32

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_CellCouette_class(const char *name) {
		return CellCouette_ns::CellCouetteClass::init(name);
	}
}


namespace CellCouette_ns
{

//+----------------------------------------------------------------------------
//
// method : 		StartCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *StartCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "StartCmd::execute(): arrived" << endl;

	((static_cast<CellCouette *>(device))->start());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		StopCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *StopCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "StopCmd::execute(): arrived" << endl;

	((static_cast<CellCouette *>(device))->stop());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		PowerOFFCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *PowerOFFCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "PowerOFFCmd::execute(): arrived" << endl;

	((static_cast<CellCouette *>(device))->power_off());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		ResetCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *ResetCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "ResetCmd::execute(): arrived" << endl;

	((static_cast<CellCouette *>(device))->reset());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		SendCommandCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *SendCommandCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "SendCommandCmd::execute(): arrived" << endl;

	Tango::DevString	argin;
	extract(in_any, argin);

	return insert((static_cast<CellCouette *>(device))->send_command(argin));
}

//+----------------------------------------------------------------------------
//
// method : 		SetMotorParamCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *SetMotorParamCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "SetMotorParamCmd::execute(): arrived" << endl;

	((static_cast<CellCouette *>(device))->set_motor_param());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		GetMotorParamCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *GetMotorParamCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "GetMotorParamCmd::execute(): arrived" << endl;

	return insert((static_cast<CellCouette *>(device))->get_motor_param());
}

//+----------------------------------------------------------------------------
//
// method : 		GetMotorStateCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *GetMotorStateCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "GetMotorStateCmd::execute(): arrived" << endl;

	return insert((static_cast<CellCouette *>(device))->get_motor_state());
}


//
//----------------------------------------------------------------
//	Initialize pointer for singleton pattern
//----------------------------------------------------------------
//
CellCouetteClass *CellCouetteClass::_instance = NULL;

//+----------------------------------------------------------------------------
//
// method : 		CellCouetteClass::CellCouetteClass(string &s)
// 
// description : 	constructor for the CellCouetteClass
//
// in : - s : The class name
//
//-----------------------------------------------------------------------------
CellCouetteClass::CellCouetteClass(string &s):DeviceClass(s)
{

	cout2 << "Entering CellCouetteClass constructor" << endl;
	set_default_property();
	get_class_property();
	write_class_property();
	
	cout2 << "Leaving CellCouetteClass constructor" << endl;

}
//+----------------------------------------------------------------------------
//
// method : 		CellCouetteClass::~CellCouetteClass()
// 
// description : 	destructor for the CellCouetteClass
//
//-----------------------------------------------------------------------------
CellCouetteClass::~CellCouetteClass()
{
	_instance = NULL;
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouetteClass::instance
// 
// description : 	Create the object if not already done. Otherwise, just
//			return a pointer to the object
//
// in : - name : The class name
//
//-----------------------------------------------------------------------------
CellCouetteClass *CellCouetteClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new CellCouetteClass(s);
		}
		catch (bad_alloc)
		{
			throw;
		}		
	}		
	return _instance;
}

CellCouetteClass *CellCouetteClass::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouetteClass::command_factory
// 
// description : 	Create the command object(s) and store them in the 
//			command list
//
//-----------------------------------------------------------------------------
void CellCouetteClass::command_factory()
{
	command_list.push_back(new StartCmd("Start",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"nothing",
		"nothing",
		Tango::OPERATOR));
	command_list.push_back(new StopCmd("Stop",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"nothing",
		"nothing",
		Tango::OPERATOR));
	command_list.push_back(new PowerOFFCmd("PowerOFF",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"nothing",
		"nothing",
		Tango::OPERATOR));
	command_list.push_back(new ResetCmd("Reset",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"nothing",
		"nothing",
		Tango::OPERATOR));
	command_list.push_back(new SendCommandCmd("SendCommand",
		Tango::DEV_STRING, Tango::DEV_STRING,
		"command to send ",
		"controller response",
		Tango::EXPERT));
	command_list.push_back(new SetMotorParamCmd("SetMotorParam",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"nothing",
		"nothing",
		Tango::OPERATOR));
	command_list.push_back(new GetMotorParamCmd("GetMotorParam",
		Tango::DEV_VOID, Tango::DEV_STRING,
		"nothing",
		"motor parameters",
		Tango::OPERATOR));
	command_list.push_back(new GetMotorStateCmd("GetMotorState",
		Tango::DEV_VOID, Tango::DEV_STRING,
		"nothing",
		"motor state",
		Tango::OPERATOR));

	//	add polling if any
	for (unsigned int i=0 ; i<command_list.size(); i++)
	{
	}
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouetteClass::get_class_property
// 
// description : 	Get the class property for specified name.
//
// in :		string	name : The property name
//
//+----------------------------------------------------------------------------
Tango::DbDatum CellCouetteClass::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		CellCouetteClass::get_default_device_property()
// 
// description : 	Return the default value for device property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum CellCouetteClass::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouetteClass::get_default_class_property()
// 
// description : 	Return the default value for class property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum CellCouetteClass::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		CellCouetteClass::device_factory
// 
// description : 	Create the device object(s) and store them in the 
//			device list
//
// in :		Tango::DevVarStringArray *devlist_ptr : The device name list
//
//-----------------------------------------------------------------------------
void CellCouetteClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{

	//	Create all devices.(Automatic code generation)
	//-------------------------------------------------------------
	for (unsigned long i=0 ; i < devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
						
		// Create devices and add it into the device list
		//----------------------------------------------------
		device_list.push_back(new CellCouette(this, (*devlist_ptr)[i]));							 

		// Export device to the outside world
		// Check before if database used.
		//---------------------------------------------
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(device_list.back());
		else
			export_device(device_list.back(), (*devlist_ptr)[i]);
	}
	//	End of Automatic code generation
	//-------------------------------------------------------------

}
//+----------------------------------------------------------------------------
//	Method: CellCouetteClass::attribute_factory(vector<Tango::Attr *> &att_list)
//-----------------------------------------------------------------------------
void CellCouetteClass::attribute_factory(vector<Tango::Attr *> &att_list)
{
	//	Attribute : mode
	modeAttrib	*mode = new modeAttrib();
	Tango::UserDefaultAttrProp	mode_prop;
	mode_prop.set_label("Mode");
	mode_prop.set_unit(" ");
	mode_prop.set_standard_unit(" ");
	mode_prop.set_display_unit(" ");
	mode_prop.set_format("%d");
	mode_prop.set_description("mode = 0 means CONTINUOUS\nmode = 1 means OSCILLATION");
	mode->set_default_properties(mode_prop);
	mode->set_memorized();
	mode->set_memorized_init(true);
	att_list.push_back(mode);

	//	Attribute : cellRadius
	cellRadiusAttrib	*cell_radius = new cellRadiusAttrib();
	Tango::UserDefaultAttrProp	cell_radius_prop;
	cell_radius_prop.set_label("R");
	cell_radius_prop.set_unit("mm");
	cell_radius_prop.set_standard_unit("mm");
	cell_radius_prop.set_display_unit("mm");
	cell_radius_prop.set_format("%6.2f");
	cell_radius_prop.set_description("is used to calculate:\n-  the distortion in oscillation mode\n- the stress torque\n- the shear rate in continuous mode");
	cell_radius->set_default_properties(cell_radius_prop);
	cell_radius->set_memorized();
	cell_radius->set_memorized_init(true);
	att_list.push_back(cell_radius);

	//	Attribute : cellGap
	cellGapAttrib	*cell_gap = new cellGapAttrib();
	Tango::UserDefaultAttrProp	cell_gap_prop;
	cell_gap_prop.set_label("dR");
	cell_gap_prop.set_unit("mm");
	cell_gap_prop.set_standard_unit("mm");
	cell_gap_prop.set_display_unit("mm");
	cell_gap_prop.set_format("%6.2f");
	cell_gap_prop.set_description("used to calculate the distortion in oscillation mode ");
	cell_gap->set_default_properties(cell_gap_prop);
	cell_gap->set_memorized();
	cell_gap->set_memorized_init(true);
	att_list.push_back(cell_gap);

	//	Attribute : positionUnity
	positionUnityAttrib	*position_unity = new positionUnityAttrib();
	Tango::UserDefaultAttrProp	position_unity_prop;
	position_unity_prop.set_label("position unity");
	position_unity_prop.set_unit(" ");
	position_unity_prop.set_standard_unit(" ");
	position_unity_prop.set_display_unit(" ");
	position_unity_prop.set_format("%d");
	position_unity_prop.set_description("used to select the position unity (discret values)\n0: STEP unity \n1: DEGREE unity\nthe conversion law is: 10000 step <=> 360�");
	position_unity->set_default_properties(position_unity_prop);
	position_unity->set_memorized();
	position_unity->set_memorized_init(true);
	att_list.push_back(position_unity);

	//	Attribute : position
	positionAttrib	*position = new positionAttrib();
	Tango::UserDefaultAttrProp	position_prop;
	position_prop.set_label("position");
	position_prop.set_unit(" ");
	position_prop.set_standard_unit(" ");
	position_prop.set_display_unit(" ");
	position_prop.set_format("%10.2f");
	position_prop.set_description("current rotor position, unity is based on position unity");
	position->set_default_properties(position_prop);
	position->set_memorized();
	position->set_memorized_init(false);
	att_list.push_back(position);

	//	Attribute : speedUnity
	speedUnityAttrib	*speed_unity = new speedUnityAttrib();
	Tango::UserDefaultAttrProp	speed_unity_prop;
	speed_unity_prop.set_label("speed unity");
	speed_unity_prop.set_unit(" ");
	speed_unity_prop.set_standard_unit(" ");
	speed_unity_prop.set_display_unit(" ");
	speed_unity_prop.set_format("%d");
	speed_unity_prop.set_description("used to select the speed unity (discret values)\n0: STEP_PER_SECOND \n1: HERTZ\n2: ROTATION_PER_MINUTE\n3: RADIAN_PER_SECOND\n4: SHEAR_RATE");
	speed_unity->set_default_properties(speed_unity_prop);
	speed_unity->set_memorized();
	speed_unity->set_memorized_init(true);
	att_list.push_back(speed_unity);

	//	Attribute : speedMax
	speedMaxAttrib	*speed_max = new speedMaxAttrib();
	Tango::UserDefaultAttrProp	speed_max_prop;
	speed_max_prop.set_label("speed max");
	speed_max_prop.set_unit(" ");
	speed_max_prop.set_standard_unit(" ");
	speed_max_prop.set_display_unit(" ");
	speed_max_prop.set_format("%8.2f");
	speed_max_prop.set_description("prevent too high speed value. unity is based on the speed unity\nattribute");
	speed_max->set_default_properties(speed_max_prop);
	speed_max->set_memorized();
	speed_max->set_memorized_init(true);
	att_list.push_back(speed_max);

	//	Attribute : isPositiveRotation
	isPositiveRotationAttrib	*is_positive_rotation = new isPositiveRotationAttrib();
	Tango::UserDefaultAttrProp	is_positive_rotation_prop;
	is_positive_rotation_prop.set_label("+ rotation");
	is_positive_rotation_prop.set_unit(" ");
	is_positive_rotation_prop.set_standard_unit(" ");
	is_positive_rotation_prop.set_display_unit(" ");
	is_positive_rotation_prop.set_description("to select either positive or negative rotation");
	is_positive_rotation->set_default_properties(is_positive_rotation_prop);
	is_positive_rotation->set_memorized();
	is_positive_rotation->set_memorized_init(false);
	att_list.push_back(is_positive_rotation);

	//	Attribute : speed
	speedAttrib	*speed = new speedAttrib();
	Tango::UserDefaultAttrProp	speed_prop;
	speed_prop.set_label("speed");
	speed_prop.set_unit(" ");
	speed_prop.set_standard_unit(" ");
	speed_prop.set_display_unit(" ");
	speed_prop.set_format("%10.2f");
	speed_prop.set_description("write part: speed preset\nif resoution = 1 => 0.01Hz < range speed < 2 Hz)\nif resoution = 64 => 2 Hz < range speed <128 Hz)\nthis value can't exceed speedMax attribute value\n\nread part: estimated value by calculation (delta_position/delta_t)");
	speed->set_default_properties(speed_prop);
	speed->set_memorized();
	speed->set_memorized_init(true);
	att_list.push_back(speed);

	//	Attribute : halfAngularAmplitude
	halfAngularAmplitudeAttrib	*half_angular_amplitude = new halfAngularAmplitudeAttrib();
	Tango::UserDefaultAttrProp	half_angular_amplitude_prop;
	half_angular_amplitude_prop.set_label("half amplitude");
	half_angular_amplitude_prop.set_unit("  ");
	half_angular_amplitude_prop.set_standard_unit("  ");
	half_angular_amplitude_prop.set_display_unit(" ");
	half_angular_amplitude_prop.set_format("%d");
	half_angular_amplitude_prop.set_description("defines the half  angular amplitude in oscillation mode as:\ntheta_min = - theta \nand\ntheta_max = + theta\nthe unity is based on position unity attribute");
	half_angular_amplitude->set_default_properties(half_angular_amplitude_prop);
	half_angular_amplitude->set_memorized();
	half_angular_amplitude->set_memorized_init(true);
	att_list.push_back(half_angular_amplitude);

	//	Attribute : frequency
	frequencyAttrib	*frequency = new frequencyAttrib();
	Tango::UserDefaultAttrProp	frequency_prop;
	frequency_prop.set_label("frequency");
	frequency_prop.set_unit("Hz");
	frequency_prop.set_standard_unit("Hz");
	frequency_prop.set_display_unit("Hz");
	frequency_prop.set_format("%6.1f");
	frequency_prop.set_description("used in WT (motor time ramp)  and WH(high hard motor speed) calculations");
	frequency->set_default_properties(frequency_prop);
	frequency->set_memorized();
	frequency->set_memorized_init(true);
	att_list.push_back(frequency);

	//	Attribute : deformation
	deformationAttrib	*deformation = new deformationAttrib();
	Tango::UserDefaultAttrProp	deformation_prop;
	deformation_prop.set_label("deformation");
	deformation_prop.set_unit(" ");
	deformation_prop.set_standard_unit(" ");
	deformation_prop.set_display_unit(" ");
	deformation_prop.set_format("%8.1f");
	deformation_prop.set_description("deformation = theta * (R+dR)/dR");
	deformation->set_default_properties(deformation_prop);
	att_list.push_back(deformation);

	//	Attribute : timeRamp
	timeRampAttrib	*time_ramp = new timeRampAttrib();
	Tango::UserDefaultAttrProp	time_ramp_prop;
	time_ramp_prop.set_label("WT");
	time_ramp_prop.set_unit("s");
	time_ramp_prop.set_standard_unit("s");
	time_ramp_prop.set_display_unit("s");
	time_ramp_prop.set_format("%6.2f");
	time_ramp_prop.set_description("- in continuous mode:\n    - write part : WT preset\n    - read part = write part\n- in oscillation mode:\n    - read part : calculated value as WT = period x (0.5 - 1/Pi) x 1000\n    (in ms) \n    - write part : no available\nto apply the preset value call the SetMotorParam Command");
	time_ramp->set_default_properties(time_ramp_prop);
	time_ramp->set_memorized();
	time_ramp->set_memorized_init(true);
	att_list.push_back(time_ramp);

	//	Attribute : speedMaxRamp
	speedMaxRampAttrib	*speed_max_ramp = new speedMaxRampAttrib();
	Tango::UserDefaultAttrProp	speed_max_ramp_prop;
	speed_max_ramp_prop.set_label("WH");
	speed_max_ramp_prop.set_unit(" ");
	speed_max_ramp_prop.set_standard_unit(" ");
	speed_max_ramp_prop.set_display_unit(" ");
	speed_max_ramp_prop.set_format("%5d");
	speed_max_ramp_prop.set_description("- in continuous mode:\n    - write part : WH preset\n    - read part = write part\n- in oscillation mode:\n    - read part : calculated value as WH = (2*Pi*theta)/T\n    - write part : no available");
	speed_max_ramp->set_default_properties(speed_max_ramp_prop);
	speed_max_ramp->set_memorized();
	speed_max_ramp->set_memorized_init(false);
	att_list.push_back(speed_max_ramp);

	//	Attribute : weightOffset
	weightOffsetAttrib	*weight_offset = new weightOffsetAttrib();
	Tango::UserDefaultAttrProp	weight_offset_prop;
	weight_offset_prop.set_label("F0");
	weight_offset_prop.set_unit("g");
	weight_offset_prop.set_standard_unit("g");
	weight_offset_prop.set_display_unit("g");
	weight_offset_prop.set_format("%8.2f");
	weight_offset_prop.set_description("capteur offset in gramms");
	weight_offset->set_default_properties(weight_offset_prop);
	att_list.push_back(weight_offset);

	//	Attribute : weight
	weightAttrib	*weight = new weightAttrib();
	Tango::UserDefaultAttrProp	weight_prop;
	weight_prop.set_label("F");
	weight_prop.set_unit("g");
	weight_prop.set_standard_unit("g");
	weight_prop.set_display_unit("g");
	weight_prop.set_format("%8.2f");
	weight_prop.set_description("F sensor measurement");
	weight->set_default_properties(weight_prop);
	weight->set_polling_period(500);
	att_list.push_back(weight);

	//	Attribute : cellHeight
	cellHeightAttrib	*cell_height = new cellHeightAttrib();
	Tango::UserDefaultAttrProp	cell_height_prop;
	cell_height_prop.set_label("H cell");
	cell_height_prop.set_unit("mm");
	cell_height_prop.set_standard_unit("mm");
	cell_height_prop.set_display_unit("mm");
	cell_height_prop.set_format("%6.2f");
	cell_height_prop.set_description("cell Height");
	cell_height->set_default_properties(cell_height_prop);
	cell_height->set_memorized();
	cell_height->set_memorized_init(true);
	att_list.push_back(cell_height);

	//	Attribute : torque
	torqueAttrib	*torque = new torqueAttrib();
	Tango::UserDefaultAttrProp	torque_prop;
	torque_prop.set_label("M");
	torque_prop.set_unit("microNm");
	torque_prop.set_standard_unit("microNm");
	torque_prop.set_display_unit("microNm");
	torque_prop.set_format("%6d");
	torque_prop.set_description("torque measurement in micro Nm");
	torque->set_default_properties(torque_prop);
	att_list.push_back(torque);

	//	Attribute : stress
	stressAttrib	*stress = new stressAttrib();
	Tango::UserDefaultAttrProp	stress_prop;
	stress_prop.set_label("stress");
	stress_prop.set_unit("Pa");
	stress_prop.set_standard_unit("Pa");
	stress_prop.set_display_unit("Pa");
	stress_prop.set_format("%8.2f");
	stress_prop.set_description("stress = M/(2*Pi*R^2*H)");
	stress->set_default_properties(stress_prop);
	att_list.push_back(stress);

	//	Attribute : temperature
	temperatureAttrib	*temperature = new temperatureAttrib();
	Tango::UserDefaultAttrProp	temperature_prop;
	temperature_prop.set_label("temperature");
	temperature_prop.set_unit("�C");
	temperature_prop.set_standard_unit("�C");
	temperature_prop.set_display_unit("�C");
	temperature_prop.set_format("%4.1f");
	temperature_prop.set_description("sample temperature");
	temperature->set_default_properties(temperature_prop);
	temperature->set_polling_period(10000);
	att_list.push_back(temperature);

	//	Attribute : motorResolution
	motorResolutionAttrib	*motor_resolution = new motorResolutionAttrib();
	Tango::UserDefaultAttrProp	motor_resolution_prop;
	motor_resolution_prop.set_label("resolution");
	motor_resolution_prop.set_unit(" ");
	motor_resolution_prop.set_standard_unit(" ");
	motor_resolution_prop.set_display_unit(" ");
	motor_resolution_prop.set_format("%d");
	motor_resolution_prop.set_description("set motor resolution:\npossible values are: 1, 2, 4, 8, 16, 32, 64.\nthe resolution has an effect on the rotor speed\n");
	motor_resolution->set_default_properties(motor_resolution_prop);
	motor_resolution->set_memorized();
	motor_resolution->set_memorized_init(true);
	att_list.push_back(motor_resolution);

	//	Attribute : viscosity
	viscosityAttrib	*viscosity = new viscosityAttrib();
	Tango::UserDefaultAttrProp	viscosity_prop;
	viscosity_prop.set_label("viscosite");
	viscosity_prop.set_format("%2.2e");
	viscosity_prop.set_description("viscosity =\n (1000*M/2*Pi*speed(Rad*sec-1)))*\n(1/ (H*R^2(R+DeltaR)/DeltaR + 4R^4/(8.465*16-H) )\n");
	viscosity->set_default_properties(viscosity_prop);
	att_list.push_back(viscosity);

	//	End of Automatic code generation
	//-------------------------------------------------------------
}

//+----------------------------------------------------------------------------
//
// method : 		CellCouetteClass::get_class_property()
// 
// description : 	Read the class properties from database.
//
//-----------------------------------------------------------------------------
void CellCouetteClass::get_class_property()
{
	//	Initialize your default values here (if not done with  POGO).
	//------------------------------------------------------------------

	//	Read class properties from database.(Automatic code generation)
	//------------------------------------------------------------------

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_class()->get_property(cl_prop);
	Tango::DbDatum	def_prop;
	int	i = -1;


	//	End of Automatic code generation
	//------------------------------------------------------------------

}

//+----------------------------------------------------------------------------
//
// method : 	CellCouetteClass::set_default_property
// 
// description: Set default property (class and device) for wizard.
//              For each property, add to wizard property name and description
//              If default value has been set, add it to wizard property and
//              store it in a DbDatum.
//
//-----------------------------------------------------------------------------
void CellCouetteClass::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;

	vector<string>	vect_data;
	//	Set Default Class Properties
	//	Set Default Device Properties
	prop_name = "SerialProxyName";
	prop_desc = "name of the serial device proxy";
	prop_def  = "";
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "TorqueCalibrationCoefficient";
	prop_desc = "TorqueCalibrationCoefficient";
	prop_def  = "1";
	vect_data.clear();
	vect_data.push_back("1");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

}
//+----------------------------------------------------------------------------
//
// method : 		CellCouetteClass::write_class_property
// 
// description : 	Set class description as property in database
//
//-----------------------------------------------------------------------------
void CellCouetteClass::write_class_property()
{
	//	First time, check if database used
	//--------------------------------------------
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("CellCouette");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("this device is used to control a specific sample environnement called Cell Couette.");
	str_desc.push_back("This equipement is consisted of 3 parts:");
	str_desc.push_back("- the motor controller");
	str_desc.push_back("- the torque controller");
	str_desc.push_back("- temperature controller");
	str_desc.push_back("");
	str_desc.push_back("the multiplexage is ensured by a multipoint link RS-485. but sometime theses controllers can be in concurrency.");
	str_desc.push_back("To improve the communication, THE STATE COMMAND MUST BE POLLED BY THE USER (under JIVE application).");
	str_desc.push_back("The other attributes which read on the hardware are polled by default.");
	str_desc.push_back("");
	str_desc.push_back("Moreover, to ensure the communication , the elapsed time between 2 RS232 requests is");
	str_desc.push_back("at least 500 ms");
	str_desc.push_back("");
	str_desc.push_back("The device communicates with controllers by using the Serial Device (RS232 communication)");
	str_desc.push_back("the communication parameters of the associated proxy device server called Serial are:");
	str_desc.push_back("Baudrate = 19200; Charlength = 8; Newline = none; Serialline = COMx; stopbits = 0; Timeout = 2000");
	str_desc.push_back("");
	str_desc.push_back("3 modes are available:");
	str_desc.push_back("CONTINUOUS: the rotor rotates continuously");
	str_desc.push_back("OSCILLATION: the rotor oscilates");
	str_desc.push_back("RAMP: the rotor rotates continuously by speed steady period");
	description << str_desc;
	data.push_back(description);
		
	//	put cvs location
	string	rcsId(RcsId);
	string	filename(classname);
	start = rcsId.find("/");
	if (start!=string::npos)
	{
		filename += "Class.cpp";
		end   = rcsId.find(filename);
		if (end>start)
		{
			string	strloc = rcsId.substr(start, end-start);
			//	Check if specific repository
			start = strloc.find("/cvsroot/");
			if (start!=string::npos && start>0)
			{
				string	repository = strloc.substr(0, start);
				if (repository.find("/segfs/")!=string::npos)
					strloc = "ESRF:" + strloc.substr(start, strloc.length()-start);
			}
			Tango::DbDatum	cvs_loc("cvs_location");
			cvs_loc << strloc;
			data.push_back(cvs_loc);
		}
	}

	//	Get CVS tag revision
	string	tagname(TagName);
	header = "$Name: ";
	start = header.length();
	string	endstr(" $");
	end   = tagname.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strtag = tagname.substr(start, end-start);
		Tango::DbDatum	cvs_tag("cvs_tag");
		cvs_tag << strtag;
		data.push_back(cvs_tag);
	}

	//	Get URL location
	string	httpServ(HttpServer);
	if (httpServ.length()>0)
	{
		Tango::DbDatum	db_doc_url("doc_url");
		db_doc_url << httpServ;
		data.push_back(db_doc_url);
	}

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("Device_4Impl");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	//--------------------------------------------
	get_db_class()->put_property(data);
}

}	// namespace
