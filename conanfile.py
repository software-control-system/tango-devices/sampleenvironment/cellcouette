from conan import ConanFile

class CellCouetteRecipe(ConanFile):
    name = "cellcouette"
    executable = "ds_CellCouette"
    version = "1.1.7"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Sandra Pierre-Joseph"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/sampleenvironment/cellcouette.git"
    description = "CellCouette device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("cpptango/9.2.5@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
